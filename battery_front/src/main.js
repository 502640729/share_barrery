import Vue from 'vue'
import App from './App.vue'
import router from './router'
import BaiDuMap from 'vue-baidu-map'
import store from './store'
import Vant from 'vant'
import Vuex from 'vuex'
import 'vant/lib/index.css'
import './assets/font/iconfont.css'
import './assets/css/base.css'
import './assets/font1/iconfont.css'
import './utils/Api'

Vue.use(Vant)
Vue.use(Vuex)
Vue.use(BaiDuMap, {
  // ak 是在百度地图开发者平台申请的密钥
  ak: '9wtqaE0T2yibM4F5LPGS4IfFPrX86DZv'
})
Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
