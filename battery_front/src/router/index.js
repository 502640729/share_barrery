import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

const routes = [
  {
    path: '/login',
    name: 'login',
    component: () => import('@/views/login/')
  },
  { // 一级路由渲染到根组件中的 router-view
    path: '/',
    component: () => import('@/views/layout/'),
    children: [ // 子路由渲染到父路由的 router-view 中
      {
        path: '/', // 默认子路由
        name: 'home',
        component: () => import('@/views/home/')
      },
      {
        path: '/my',
        name: 'my',
        component: () => import('@/views/my/')
      }
    ]
  },
  {
    path: '/home/shop',
    name: 'shop',
    component: () => import('@/views/shop')
  },
  {
    path: '/service',
    name: 'Service',
    component: () => import('@/views/service')
  },
  {
    path: '/wallet',
    name: 'wallet',
    component: () => import('@/views/my/wallet/')
  },
  {
    path: '/money',
    name: 'money',
    component: () => import('@/views/my/wallet/money/')
  },
  {
    path: '/deal',
    name: 'deal',
    component: () => import('@/views/my/wallet/deal/')
  },
  {
    path: '/agreement',
    name: 'agreement',
    component: () => import('@/views/my/wallet/money/agreement/')
  },
  {
    path: '/user/ticket',
    name: 'Coupon',
    component: () => import('@/views/my/coupon/Coupon')
  },
  {
    path: '/user/message',
    name: 'Message',
    component: () => import('@/views/my/message/Message')
  },
  {
    path: '/user/order',
    name: 'Order',
    component: () => import('@/views/my/order/Order')
  },
  {
    path: '/apipayquestion', // 默认子路由
    name: 'Apipayquestion',
    component: () => import('../views/home/apipayquestion/Apipayquestion')
  },
  {
    path: '/costerror', // 默认子路由
    name: 'Costerror',
    component: () => import('../views/home/costquestion/Costerror')
  },
  {
    path: '/glitch',
    name: 'Glitch',
    component: () => import('../views/home/usequestion/Glitch')
  },
  {
    path: '/usequestion',
    name: 'Usequestion',
    component: () => import('../views/home/usequestion/Usequestion1')
  },
  {
    path: '/usequestions',
    name: 'Usequestions',
    component: () => import('../views/home/usequestion/Usequestion2')
  },
  {
    path: '/returnquestion',
    name: 'Returnquestion',
    component: () => import('../views/home/returnquestion/Returnquestion1')
  },
  {
    path: '/returnquestions',
    name: 'Returnquestions',
    component: () => import('../views/home/returnquestion/Returnquestion2')
  },
  {
    path: '/costquestiono',
    name: 'Costquestiono',
    component: () => import('../views/home/costquestion/Costquestion1')
  },
  {
    path: '/costquestiont',
    name: 'Costquestiont',
    component: () => import('../views/home/costquestion/Costquestion2')
  },
  {
    path: '/costquestions',
    name: 'Costquestions',
    component: () => import('../views/home/costquestion/Costquestion3')
  },
  {
    path: '/noreturn',
    name: 'Noreturn',
    component: () => import('../views/home/returnquestion/Noreturn')
  },
  {
    path: '/return',
    name: 'Return',
    component: () => import('../views/home/return/Return')
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
