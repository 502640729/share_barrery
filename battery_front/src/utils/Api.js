import axios from 'axios'
import Vue from 'vue'
import qs from 'qs'

// 在非组件模块中获取 store 必须通过这种方式
// 这里单独加载 store, 和在组件中 this.$store 是一个东西
// import store from '@/store/'

// axios.defaults.headers['Content-Type'] = 'application/x-www-form-urlencoded'; // 设置默认的请求头的Content-Type

axios.interceptors.request.use(config => {
  if (sessionStorage.getItem('tokenStr')) {
    config.headers.Authorization = sessionStorage.getItem('tokenStr')
  }
  config.headers.Authorization = 'eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJhZG1pbiIsImNyZWF0ZWQiOjE2MjkzMzc3MTEzNTEsImV4cCI6MTYyOTk0MjUxMX0.FSTmiNgRKQV4zLYldvzv_TfJiG3avVBqrN64KJVJthw'

  /* const { user } = store.state

  // 如果用户已登录，统一给接口设置 token 信息
  if (user) {
    config.headers.Authorization = `${user.token}`
  } */

  config.baseURL = '/api1'
  // 如果存在token,请求就会携带这个token
  return config
}, error => {
  console.log(error)
})

/**
 * @description 封装的get请求的方法
 * @param {*} url 请求的地址
 * @param {*} data  请求的数据
 * @returns 数据请求的promise对象
 */
export function getRequest (url, data) {
  return axios.get(url, {
    params: data
  })
}

/**
 * @description 封装post请求的方法，传参json类型数据
 * @param {*} url 请求的地址
 * @param {*} data 请求的数据
 * @returns 数据请求的promise对象
 */
export function postRequestJson (url, data) {
  return axios.post(url, data)
}

/**
 * @description 封装post请求的方法， 传参form表单类型数据
 * @param {*} url 请求的地址
 * @param {*} data 请求的数据
 * @returns 数据请求的promise对象
 */
export function postRequestForm (url, data) {
  return axios({
    method: 'post',
    url,
    data: qs.stringify(data)
  })
}

/**
 * @description 封装delete请求的方法
 * @param {*} url 请求的地址
 * @param {*} data 请求的数据
 * @returns 数据请求的promise对象
 */
export function deleteRequest (url, data) {
  return axios.delete(url, data)
}

Vue.prototype.postRequestJson = postRequestJson
Vue.prototype.postRequestForm = postRequestForm
Vue.prototype.getRequest = getRequest
Vue.prototype.deleteRequest = deleteRequest
