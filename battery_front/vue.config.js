module.exports = {
  devServer: {
    proxy: {
      '/api1': { // 匹配所有以 '/api1'开头的请求路径
        target: 'http://123.56.158.143:8250', // 代理目标的基础路径
        changeOrigin: true,
        pathRewrite: { '^/api1': '' }
      }
    }
  }
}
