import './App.css';
import Home from "./component/Home"
import Login from "./component/Login";
import './mock/Mock'
import React,{Component} from 'react'
import zhCN from 'antd/lib/locale/zh_CN';
import {ConfigProvider} from "antd";
import {Route, withRouter} from "react-router-dom";
class App extends Component {
    render() {
        console.log(this.props)
        return (
            <ConfigProvider locale={zhCN}>
                <div className="App">
                    {
                        sessionStorage.getItem("tokenStr") === null ? <Login /> : <Home />
                    }
                    <Route path={'/home'} component={Home} />
                </div>
            </ConfigProvider>
        );
    }
}

export default withRouter(App);
