import React from 'react'
import {Card} from 'antd'
//导入主题
import echartTheme from './../echartTheme'
import themeLight from './../themeLight'
//全部加载
// import echarts from 'echarts'
//按需加载
import echarts from 'echarts/lib/echarts'
//必需基础组件
import 'echarts/lib/component/tooltip'
import 'echarts/lib/component/title'
import 'echarts/lib/component/legend'
import 'echarts/lib/component/markPoint'
//导入矩形图
import 'echarts/lib/chart/bar'
import ReactEcharts from 'echarts-for-react'

export default class Bar extends React.Component{

    componentWillMount(){
        echarts.registerTheme = function (default1, echartTheme) {

        }
        echarts.registerTheme('Default', echartTheme);
        echarts.registerTheme('Light', themeLight);
    }

    getOption = () => {
        let option = {
            title: {
                text: '用户租借订单数统计图表',
                x: 'center'
            },
            tooltip: {//提示条
                trigger: 'axis'
            },
            xAxis: {  //X轴
                data: ['周一','周二','周三','周四','周五','周六','周日']
            },
            yAxis: {  //Y轴
                type: 'value'
            },
            series: [ //整体数据源
                {
                    name: '订单量',
                    type: 'bar',
                    data: [1000, 2000, 1500, 3000, 2000, 1200, 800]
                }
            ]
        }
        return option;
    }
    getOption2 = () => {
        let option = {
            // title: {
            //     text: '用户租借订单',
            //     x: 'center'  //水平方向居中
            // },
            legend: {
                orient: 'vertical', //垂直方向排列
                right: 10,          //绝对定位位置
                top: 20,
                bottom: 20,
                data: ['周一','周二','周三','周四','周五','周六','周日']
            },
            tooltip:{
                trigger: 'item',
                formatter: '{a}<br/>{b}:{c}({d}%)' //格式化提示项
            },
            series: [
                {
                    name: '订单量',
                    type: 'pie',
                    data: [
                        {
                            value: 1000,
                            name: '周一'
                        },
                        {
                            value: 1000,
                            name: '周二'
                        },
                        {
                            value: 2000,
                            name: '周三'
                        },
                        {
                            value: 1500,
                            name: '周四'
                        },
                        {
                            value: 3000,
                            name: '周五'
                        },
                        {
                            value: 2000,
                            name: '周六'
                        },
                        {
                            value: 1200,
                            name: '周日'
                        }
                    ]
                }
            ]
        }
        return option;
    }
    getOption3 = () => {
        let option = {
            // title: {
            //     text: '用户租借订单'
            // },
            tooltip: {
                trigger: 'axis'
            },
            xAxis: {
                boundaryGap: false, //坐标轴从起点开始，true时在中间
                data: ['周一','周二','周三','周四','周五','周六','周日']
            },
            yAxis: {
                type: 'value'
            },
            series: [
                {
                    name: '订单量',
                    type: 'line',
                    data: [1000,2000,1500,3000,2000,1200,800], //趋势点
                    areaStyle: {} //区域填充颜色
                }
            ]
        }
        return option;
    }

    render(){
        return(
           <div>
               <Card title="柱状图展示">
                  <ReactEcharts option={this.getOption()} theme="Default" style={{height: 500}}/>
               </Card>
               <Card title="饼形图展示">
                   <ReactEcharts option={this.getOption2()} theme="Light"/>
               </Card>
               <Card title="区域折线图展示">
                   <ReactEcharts option={this.getOption3()} theme="Default" style={{height: 500}}/>
               </Card>
           </div>
        )
    }
}
