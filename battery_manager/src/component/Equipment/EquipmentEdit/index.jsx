import React from "react"
import 'antd/dist/antd.css';
import {Space, Card, Input, Form, Select, Button, message, Upload} from 'antd'
import http from "../../../utils/Api";
import {withRouter} from "react-router-dom";
import {UploadOutlined} from "@ant-design/icons";
const {Option} = Select
const {TextArea}=Input
class EquipmentEdit extends React.Component{
    constructor(props) {
        super(props);
        this.state = {
            equipmentInfo: {
                ...this.props.location.state.equipmentInfo,
                picture: ''
            },
            shopList:[],
            pagination: {
                total: 0
            }
        }
    }
    getShopList=()=>{
        http.getRequest('/fro/shop/list').then(({data})=>{
            if (data.code === 200){
                this.setState({
                    shopList:data.data
                })
            }
        })
    }
    addEquipment=(value)=> {
        console.log(this.state)
        this.setState({
            equipmentInfo: {
                ...value,
                picture: this.state.equipmentInfo.picture,
                id: this.state.equipmentInfo.id,
            }
        },() => {
            console.log('$$$$$$',this.state.equipmentInfo)
            http.postRequest_json('/sys/equipment/edit',this.state.equipmentInfo).then(({data}) => {
                console.log(data)
                console.log(this.props)
                if (data.code === 200) {
                    message.success(data.message,).then(()=> {
                        this.props.history.replace('/equipment')
                    })
                }
            })
        })
    }

    componentDidMount() {
        this.getShopList();
    }

    render() {
        return(
            <div>

                <Space direction="vertical" style={{width:"100%"}}>
                    <Card title="添加设备信息">
                        <Form
                            onFinish={this.addEquipment}
                            name="basic"
                            labelCol={{
                                span: 2,
                            }}
                            wrapperCol={{
                                span: 22,
                            }}
                            initialValues={
                                {
                                    ...this.props.location.state.equipmentInfo
                                }
                            }
                        >
                            <Form.Item
                                label="设备名称"
                                name="name"
                                rules={[
                                    {
                                        required: true,
                                        message: '请输入你的设备名称',
                                    },]}>
                                <Input id="name" size="large" placeholder="请输入设备名称"/>
                            </Form.Item>
                            <Form.Item
                                label="备注"
                                name="remark"
                                rules={[
                                    {required:true,message:"请添加备注"}
                                ]}>
                                <TextArea  placeholder="请添加备注"/>
                            </Form.Item>
                            <Form.Item
                                label="设备容量"
                                name="count"
                                rules={[
                                    {required:true,message:"请输入设备容量"}
                                ]}>
                                <Input size="large" placeholder="请输入设备容量"/>
                            </Form.Item>
                            <Form.Item
                                label="所属店铺"
                                name="shopId"
                                rules={[
                                    {required:true,message:"请选择店铺"}
                                ]}>
                                <Select>
                                    {
                                        this.state.shopList.map(item => {
                                            return (
                                                <Option value={item.id}>{item.name}</Option>
                                            )
                                        })
                                    }
                                </Select>
                            </Form.Item>
                            <Form.Item label="设备图片">
                                <Upload
                                    action="/api1/upload"
                                    listType="picture"
                                    onChange={(e) => {
                                        console.log('@@@@@',e)
                                        if (e.file.status === 'done') {
                                            if (e.file.response.code === 200) {
                                                //说明返回成功了
                                                this.setState({
                                                    equipmentInfo: {
                                                        ...this.state.equipmentInfo,
                                                        picture: e.file.response.data.fileName
                                                    }
                                                },() => {
                                                    console.log('@@@@@@@@@@@@',this.state)
                                                })
                                            }
                                        }
                                    }}
                                >
                                    <Button icon={<UploadOutlined />}>上传</Button>
                                </Upload>
                            </Form.Item>
                            <Form.Item label="设备状态" name={"status"} rules={[{ required: true }]}>
                                <Select defaultValue={0} style={{ width: 100 }} onChange={(e) => {this.setState({
                                    params: {
                                        ...this.state.params,
                                        status: e
                                    }
                                })}}>
                                    <Option value={0}>所有状态</Option>
                                    <Option value={1}>可用状态</Option>
                                    <Option value={2}>禁用状态</Option>
                                </Select>
                            </Form.Item>
                            <Form.Item>
                                <Button type="primary" htmlType="submit" style={{marginRight:0}}>提交</Button>
                            </Form.Item>
                        </Form>
                    </Card>
                </Space>
            </div>
        )
    }
}
export default withRouter(EquipmentEdit)
