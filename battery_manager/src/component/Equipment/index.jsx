import React from 'react'

import {
    Button,
    Pagination,
    Table,
    message,
    Input,
    Modal,
    Select,
} from 'antd';

import {ExclamationCircleOutlined} from "@ant-design/icons";
import {Option} from "antd/es/mentions";

import baseUrl from "../../utils/baseUrl";

import http from '../../utils/Api'
import {NavLink} from "react-router-dom";

const params={
    name:'',
    status:0,
    pageSize:10,
    currentPage:1
}

export default class Equipment extends React.Component {

    state={
        EquipmentList:[],
        pagination: {
            total: 0,
            currentPage: 1
        },
        params: {...params}
    }
    getEquipmentList=()=>{
        http.getRequest('/sys/equipment/list',this.state.params).then(({data})=>{
            if (data.data.length >= 0) {
                message.success({content: '数据获取成功'}).then(r => {})
                this.setState({
                    EquipmentList:data.data,
                    pagination:{
                        ...this.state.pagination,
                        total:data.total
                    }
                })
            }
        })
    }
    /* 删除按钮，点击弹出 */
    showDeleteConfirm = (id) => {
        Modal.confirm({
            title: "你确定要删除吗?",
            icon: <ExclamationCircleOutlined/>,
            okText: "确定",
            okType: "danger",
            cancelText: "取消",
            onOk: () => {
                this.deleteEquipment(id)
            }
        });
    };
    /* 删除按钮的方法，被onOk调用 */
    deleteEquipment = (id) => {
        http.deleteRequest(`/sys/equipment/delete/${id}`).then(({data}) => {
            if (data.code===200) {
                console.log(data);
                this.getEquipmentList()
            }
        })
        console.log(id);
    }
    /* 状态更改方法 */
    UpdateDisabled = (status,item) => {
        http.postRequest_form('/sys/equipment/status',{status,id: item.id}).then(({data})=> {
            console.log(data)
            if (data.code === 200) {
                message.success(data.message)
                this.getEquipmentList()
            }

        })
        console.log(item);

    }
    /* 状态更改的弹窗 */
    showUpdateConfirm = (status,item) => {
        Modal.confirm({
            title: "你确定要修改吗?",
            icon: <ExclamationCircleOutlined/>,
            okText: "确定",
            okType: "danger",
            cancelText: "取消",
            onOk: () => {
                status = status === 1 ? 2 : 1
                this.UpdateDisabled(status,item)
            }
        });
    }
    /* 通过状态查询设备 */
    searchEquipmentByStatus = () => {
        this.getEquipmentList()
    }
    statusChange=(e)=>{
        this.setState({
            params:{
                ...this.state.params,
                status:e
            }
        })
    }
    nameChange=(e)=>{
        this.setState({
            params:{
                ...this.state.params,
                name:e.target.value
            }
        })
    }
    /*更改一页显示条数*/
    changePage=(page,pageSize)=>{
        console.log(page,pageSize)
        this.setState({
            params:{
                ...this.state.params,
                pageSize:pageSize,
                currentPage:page
            },
            pagination: {
                ...this.state.pagination,
                currentPage: page
            }
        },()=>{
            this.getEquipmentList()
        })

    }
    componentDidMount=()=> {
        this.getEquipmentList()
    }

    render() {
        const columns=[
            { title:"设备编号", dataIndex:"id"},
            { title:"设备名称", dataIndex:"name"},
            { title:"设备备注", dataIndex:"remark"},
            { title:"设备创建时间", dataIndex:"createTime"},
            { title:"设备更新时间", dataIndex:"updateTime"},
            { title:"设备容量", dataIndex:"count"},
            { title:"设备照片", dataIndex:"picture",render: (picture) => <img src={baseUrl+'/getImgStream?imgPath='+ picture} alt="" width="60px"/>},
            { title:"所属店铺", dataIndex: "froShop",render: (froShop) => {
                    return froShop.name;
                }},
            {title: "设备状态",dataIndex: "status", render: (status,item) => <Button type={item.status === 1 ? 'primary' : 'danger'} onClick={() => this.showUpdateConfirm(status,item)}>{item.status === 1 ? '可用' : '禁用'}</Button>},
            {title: "操作",key:"opt",width: '200px', render:(row)=>{
                    return(
                        <>
                            <Button type={"primary"}
                                    onClick={()=>this.props.history.push('/equipment/edit',{equipmentInfo: row})}>编辑</Button>
                            &nbsp;
                            <Button type={"danger"} onClick={()=>{this.showDeleteConfirm(row.id)}}>删除</Button>
                        </>
                    )
                }}
        ]
        return (
            <div>
                <Input size="large" placeholder="查询设备" name={"name"} allowClear style={{width:500,marginRight:20}} onChange={this.nameChange}/>
                <Select dropdownClassName={'statusMessage'} defaultValue={this.state.params.status} style={{ width: 120,marginRight:20 }} onChange={this.statusChange} size="large" name={"status"}>
                    <Option value={0}>所有状态</Option>
                    <Option value={1}>可用</Option>
                    <Option value={2}>禁用</Option>
                </Select>
                <Button size="large" type={"primary"} style={{marginRight:20}} onClick={this.searchEquipmentByStatus} >查询</Button>
                <Button type="primary" size="large"><NavLink to={{pathname:'/equipment/edit',state: {equipmentInfo: {id: 0}}}}>添加设备</NavLink></Button>
                <p/>
                <Table scroll={{ y: 500 }} className={"table"} dataSource={this.state.EquipmentList} columns={columns} onChange={this.getEquipmentList} pagination={false}/>
                <p/>
                <Pagination
                    showSizeChanger
                    defaultCurrent={params.currentPage}
                    defaultPageSize={params.pageSize}
                    total={this.state.pagination.total}
                    onChange={this.changePage}
                    showTotal={total => `共${total} 条数据`}
                />
            </div>
        );
    }
}
