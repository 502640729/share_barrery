import React,{Component} from "react";
import { Layout } from 'antd';
import MyHeader from "../MyLayout/MyHeader";
import MySider from "../MyLayout/MySider";
import MyContent from "../MyLayout/MyContent";
export default class Home extends Component {
    render() {
        return (
            <div>
                <Layout style={{display: 'flex',flexDirection: 'column',height: '100vh'}}>
                    <MyHeader />
                    <Layout style={{display: 'flex',flexDirection: 'row',flexGrow: 1}} >
                        <MySider />
                        <MyContent />
                    </Layout>
                </Layout>
            </div>
        );
    }
}
