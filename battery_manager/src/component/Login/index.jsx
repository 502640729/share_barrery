import React from "react"
import {Form,Input,Button,message,Image} from "antd"
import http from "../../utils/Api";
import { EyeInvisibleOutlined, EyeTwoTone } from '@ant-design/icons';
import {nanoid} from 'nanoid'
import {withRouter} from 'react-router-dom'
class Login extends React.Component{
    constructor(props) {
        super(props);
        this.state={
            loginInfo:{
                captcha: "",
                password: "",
                username: "",
                uuid: localStorage.getItem('uuid')
            },
            captcha: '',
        }
    }
    checkCaptcha (val) {
        return val.length === 4
    }
    getCaptcha() {
        this.setState({
            captcha: 'http://123.56.158.143:8250/captcha?uuid=' + this.state.loginInfo.uuid + '&time='+ new Date()
        })
    }
    componentDidMount() {
        console.log(this.props)
        const uuid = nanoid();
        localStorage.setItem('uuid',uuid);
        this.getCaptcha()
    }

    login=(value) => {
        console.log('@@@@', value)
        this.setState({
            loginInfo: {
                ...this.state.loginInfo,
                ...value
            }
        },()=> {
            http.postRequest_json("/sys/user/login",this.state.loginInfo).then(({data})=>{
                if (data.code===200){
                    sessionStorage.setItem('tokenStr',data.data.token)
                    message.success('登陆成功').then(()=> {
                        console.log(sessionStorage.getItem('tokenStr'))
                        this.props.history.replace('/home')
                    })
                }
            })
        })
    }

    render() {
        console.log(this.state.captcha);
        return (
            <div style={{background:"url('https://z3.ax1x.com/2021/08/21/fxZHxK.jpg') center/cover",height: '100vh',width: '100%',display: "flex",justifyContent: "center",alignItems: "center"}}>
                <div style={{width:"40%",padding:"0 20px",height:"40%",backgroundColor: "white",opacity: '.8',borderRadius: '20px',textAlign: "center",color: "black"}}>
                    <h2>登&nbsp;&nbsp;录</h2>
                    <Form onFinish={this.login}
                          labelCol={{span:4}}
                          wrapperCol={{span:20}}>
                        <Form.Item
                            label="用户名"
                            name="username">
                            <Input placeholder="请输入用户名" size="middle" id="username"/>
                        </Form.Item>
                        <Form.Item
                            label="密&nbsp;&nbsp;&nbsp;&nbsp;码"
                            name="password">
                            <Input.Password
                                placeholder="请输入登录密码"
                                iconRender={visible => (visible ? <EyeTwoTone /> : <EyeInvisibleOutlined />)}
                            />
                        </Form.Item>
                        <Form.Item
                            label="验证码"
                            name="captcha">
                            <Input placeholder="请输入验证码" size="middle" id="captcha" suffix={<Image src={this.state.captcha} style={{width:"50px", height:"30px"}} onClick={this.getCaptcha.bind(this)}/>}/>

                        </Form.Item>
                        <Form.Item style={{margin:"0 auto"}}>
                            <Button type={"primary"} htmlType={'submit'} >提交</Button>
                        </Form.Item>
                    </Form>
                </div>

            </div>
        )
    }

}
export default withRouter(Login)
