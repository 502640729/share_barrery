import React,{Component} from 'react'
import http from "../../../utils/Api";
import { Table,Input,Button, Avatar, Modal, Pagination} from 'antd';
import { NavLink} from "react-router-dom";
const { confirm } = Modal;
const { Search } = Input;
export default class MessageList extends Component {
    state = {
        userList: [],
        params: {
            pageSize: 10,
            currentPage: 1,
            status: '',
            username:'',
            phone:""
        },
        pagination: {
            total: 0
        }
    }
    getUserList=() => {
        const {params} = this.state;
        http.getRequest('/sys/user/list', params).then(({data})=> {
            console.log(data)
            this.setState({
                userList:data.data,
                pagination: {
                    total: data.total
                }
            })
        })
    }
    changeStatus=(status,item)=> {
        const {id} = item
        const obj = {
            id,
            status,
        }
        console.log(obj)
        Modal.confirm({
            title: '提示框',
            content: '确认更改该用户的状态么?',
            okText: '确认',
            cancelText: '取消',
            onOk: () => {
                http.postRequest('/sys/user/status', obj).then(({data})=> {
                    console.log(data)
                })
            }
        })
    }
    searchName(username){
        this.setState({
            params: {
                ...this.state.params,
                username
            }
        },() => {
            console.log('@',this.state.params)
            this.getUserList()
        })
    }
    searchPhone(phone){
        this.setState({
            params:{
                ...this.state.params,
                phone
            }
        },()=>{
            this.getUserList()
        })
    }
    componentDidMount() {
        console.log(this.props)
        this.getUserList()
    }

    /*编辑信息*/
    edit(Id){
        this.setState({
            setEditingId:Id
        })
    }
    /*更改一页显示条数*/
    changePage(page,pageSize){
        this.setState({
            params:{
                ...this.state.params,
                pageSize:pageSize,
                currentPage:page
            }
        },()=>{
            this.getUserList()
        })

    }

    /*删除信息*/
    handleDelete(Id){
        confirm({
            title:"你确定删除该用户信息吗？",
            content: "你确定删除该用户信息吗？",
            onOk() {
                http.deleteRequest(`/sys/user/delete/${Id}`).then(({data})=>{
                    if (data.code===200){
                        console.log(data)
                        this.getUserList()
                    }
                })
            },
            onCancel() {
                console.log('Cancel');
            },
        })

    }
    render() {
        return (
            <div>
                <Search style={{width:"20%"}}
                        placeholder="用户名查询"
                        enterButton="查询"
                        size="large"
                        allowClear
                        onSearch={this.searchName.bind(this)}
                />
                <Search style={{width:"20%"}}
                        placeholder="手机号查询"
                        enterButton="查询"
                        size="large"
                        allowClear
                        onSearch={this.searchPhone.bind(this)}
                />
                <Button type={"primary"} style={{height:"40px"}}>
                    <NavLink to={"/user/edit"}>添加用户</NavLink>
                </Button>

                <Table dataSource={this.state.userList}
                       columns={[
                           {title:"编号", dataIndex:"id"},
                           {title:"用户名", dataIndex:"username"},
                           {title:"昵称", dataIndex:"nickname"},
                           {title:"手机号", dataIndex:"phone"},
                           {title:"头像", dataIndex:"avatar", render: (avatar) => {
                                   return (
                                       <Avatar size={64} src={avatar} />
                                   )
                               }},
                           {title:"角色名称", dataIndex:"role",render: (role) => {
                                   return role.name;
                               }},
                           {title:"用户备注", dataIndex:"remark"},
                           {title: "用户状态", dataIndex: 'status',
                               render: (status,item) => {
                                   return (
                                       {status} === 1 ?
                                           <Button onClick={()=> this.changeStatus(1,item)} type={'danger'}>禁用</Button>
                                           : <Button onClick={()=> this.changeStatus(2,item)} type={'primary'}>可用</Button>
                                   )
                               }},
                           {title:"删除",render:()=>{
                                   return(
                                       <Button type={"danger"}
                                               onClick={()=>this.handleDelete(this.state.userList.id)}>删除</Button>
                                   )
                               }},
                           {title:"编辑",render:()=>{
                                   return(
                                       <Button type={"primary"}
                                               onClick={()=>this.edit(this.state.userList.id)}>编辑</Button>
                                   )
                               }}
                       ]}
                       pagination={false} />
                <>
                    <Pagination
                        total={this.state.pagination.total}
                        current={this.state.currentPage}
                        showSizeChanger
                        showQuickJumper
                        showTotal={total => `共 ${total} 条`}
                        onChange={()=>this.changePage(this.state.currentPage,this.state.pageSize)}
                    />
                </>
            </div>
        );
    }
}
