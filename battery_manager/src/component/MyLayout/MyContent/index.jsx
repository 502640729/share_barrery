import React,{Component} from 'react'
import { Layout,PageHeader } from 'antd';
import {Switch, Route, Redirect, withRouter} from "react-router-dom";
import UserList from "../../User/UserList";
import BarryList from "../../Barry/BarryList";
import Equipment from "../../Equipment";
import VipList from "../../Vip/VipList";
import AddUser from "../../User/AddUser";
import MessageList from "../../Message/MessageList"
import AddVip from '../../Vip/AddVip'
import EquipmentEdit from "../../Equipment/EquipmentEdit";
import Login from '../../Login'
import Bar from "../../ECharts/Bar";
import Role from "../../Role";
import Shop from "../../Shop"
import ShopEdit from "../../Shop/ShopEdit"

const { Content } = Layout;
class MyContent extends Component {
    render() {
        console.log('myContent', this.props);
        const {title} = this.props.location.state || {title: '首页'}
        return (
            <Layout>
                <PageHeader title={title} onBack={()=>this.props.history.goBack()}/>
                <Content
                    className="site-layout-background"
                    style={{
                        padding: 25,
                        margin: 0,
                        minHeight: 280,
                        overflow: 'auto'
                    }}
                >
                    <Switch>
                        <Route path="/user/list" component={UserList} />
                        <Route path="/barry/list" component={BarryList} />
                        <Route path="/equipment" exact component={Equipment} />
                        <Route path="/vip/list" exact component={VipList} />
                        <Route path="/equipment/edit" component={EquipmentEdit} />
                        <Route path="/vip/list" component={VipList} />
                        <Route path="/user/edit" component={AddUser} />
                        <Route path="/vip/edit" component={AddVip} />
                        <Route path="/message" component={MessageList} />
                        <Route path="/login" component={Login} />
                        <Route path="/shop" exact component={Shop} />
                        <Route path="/shop/edit" component={ShopEdit} />
                        <Route path="/role/list" component={Role}/>
                        <Route path="/echarts" exact component={Bar}/>
                        <Redirect to={'/echarts'} />
                    </Switch>
                </Content>
            </Layout>
        );
    }
}

export default withRouter(MyContent)
