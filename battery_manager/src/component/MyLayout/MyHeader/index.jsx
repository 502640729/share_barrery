import React,{Component} from 'react'
import { Layout } from 'antd';
import header from './index.module.css'
const { Header } = Layout;

export default class MyHeader extends Component {
    render() {
        return (
            <Header>
                <div className={'logo'}>
                    <h1 className={header.title}>妖怪充电宝后台管理系统</h1>
                </div>
            </Header>
        );
    }
}
