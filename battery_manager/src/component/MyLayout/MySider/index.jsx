import React,{Component} from 'react'
import {Layout,Menu} from "antd";
import { LaptopOutlined,DesktopOutlined, ThunderboltOutlined,SettingOutlined ,TeamOutlined ,UserOutlined,ShopOutlined} from '@ant-design/icons';
import {NavLink, withRouter} from "react-router-dom";
const {Sider} = Layout
const { SubMenu } = Menu;

class MySider extends Component {

    render() {
        const {pathname} = this.props.location
        console.log(pathname)
        console.log(pathname.substr(1).split('/')[0])
        return (
            <div style={{overflow: "auto"}}>
                <Sider theme={'dark'} collapsible style={{overflow: 'auto',height: '100%'}} width={200} className="site-layout-background">
                    <Menu
                        mode="inline"
                        theme="dark"
                        defaultSelectedKeys={pathname}
                        defaultOpenKeys={[pathname.substr(1).split('/')[0]]}
                        style={{overflow: 'auto'}}
                    >
                        <Menu.Item icon={<DesktopOutlined />}><NavLink to={{pathname: '/home', state: {'title': '控制台'}}}>控制台</NavLink></Menu.Item>
                        <SubMenu key="user" icon={<LaptopOutlined />} title="用户管理">
                            <Menu.Item key="/user/list">
                                <NavLink to={{pathname: '/user/list', state: {'title': '用户列表'}}}>用户列表</NavLink>
                            </Menu.Item>
                        </SubMenu>
                        <SubMenu key="barry" icon={<ThunderboltOutlined />} title="充电宝管理">
                            <Menu.Item key="/barry/list">
                                <NavLink to={{pathname: '/barry/list', state:{ 'title': '充电宝列表' }}}>充电宝列表</NavLink>
                            </Menu.Item>
                        </SubMenu>
                        <SubMenu key="equipment" icon={<SettingOutlined />} title="设备管理">
                            <Menu.Item key="/equipment">
                                <NavLink to={{pathname:'/equipment',state:{'title':'设备列表'}}}>设备列表</NavLink>
                            </Menu.Item>
                        </SubMenu>
                        <SubMenu key="shop" icon={<ShopOutlined />} title="店铺管理">
                            <Menu.Item key="/shop">
                                <NavLink to={{pathname:'/shop',state:{'title':'店铺列表'}}}>店铺列表</NavLink>
                            </Menu.Item>
                        </SubMenu>
                        <SubMenu key="vip" icon={<UserOutlined />} title="会员管理">
                            <Menu.Item key="/vip/list">
                                <NavLink to={{pathname:'/vip/list',state:{'title':'会员列表'}}}>会员列表</NavLink>
                            </Menu.Item>
                        </SubMenu>
                        <SubMenu key="role" icon={<TeamOutlined />} title="角色管理">
                            <Menu.Item key="/role/list">
                                <NavLink to={{pathname:'/role/list',state:{'title':'角色管理'}}}>角色管理</NavLink>
                            </Menu.Item>
                        </SubMenu>
                    </Menu>
                </Sider>
            </div>
        );
    }
}

export default withRouter(MySider)
