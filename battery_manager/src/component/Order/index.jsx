import React,{Component} from 'react'
import {Button, Form, Input, Modal, Pagination, Table} from "antd";
import http from "../../utils/Api";

class Order extends Component {
    constructor(props) {
        super(props);
        this.state={
            orderList:[],
            params: {
                pageSize: 10,
                currentPage: 1,
                name: '',
                username:'',
            },
            pagination: {
                total: 0,
                currentPage: 1
            }
        }
    }
    getOrderList(){
        const {params}=this.state
        http.getRequest("/fro/order/list",params).then(({data})=>{
            this.setState({
                orderList:data.data,
                pagination: {
                    ...this.state.pagination,
                    total: data.total
                }
            })
        })
    }
    changeStatus=(status,item)=> {
        const obj = {
            id: item.id,
            status: status === 1 ? 2 : 1
        }
        this.getOrderList()
        Modal.confirm({
            title: '提示框',
            content: '确认删除该订单么?',
            okText: '确认',
            cancelText: '取消',
            onOk: () => {
                alert("删除成功")
            }
        })
    }
    /*更改一页显示条数*/
    changePage=(page,pageSize)=>{
        this.setState({
            params:{
                ...this.state.params,
                pageSize:pageSize,
                currentPage:page
            },
            pagination: {
                ...this.state.pagination,
                currentPage: page
            }
        },()=>{
            this.getUserList()
        })

    }
    componentDidMount() {
        this.getOrderList()
    }
    render() {
        return (
            <div>
                <Form layout={"inline"} style={{marginBottom: '30px'}}>
                    <Form.Item>
                        <Input placeholder="订单名称" name={'name'} onChange={(e)=> {
                            this.setState({
                                queryParams: {...this.state.queryParams,name: e.target.value},

                            })
                        }} />
                    </Form.Item>
                    <Form.Item>
                        <Button type="primary" onClick={this.getRoleList}>查询</Button>
                    </Form.Item>
                    <Form.Item>
                        <Button type="primary" onClick={()=>{this.setState({visible: true})}}>添加角色</Button>
                    </Form.Item>

                </Form>
                <Table scroll={{ y: 500 }} dataSource={this.state.orderList}
                       columns={[
                           {title:"编号", dataIndex:"id"},
                           {title:"用户id", dataIndex:"userId"},
                           {title:"订单编号", dataIndex:"orderId"},
                           {title:"充电宝编号", dataIndex:"batteryId"},
                           {title: "用户状态", dataIndex: 'status',
                               render: (status,item) => {
                                   return (
                                       <Button
                                           type={item.status === 1 ? 'primary' : 'danger'} onClick={() => this.changeStatus(status,item)}>{item.status === 1 ? '删除' : '已删除'}</Button>
                                   )
                               }},
                       ]}
                       pagination={false} />
                       <>
                           <Pagination
                               total={this.state.pagination.total}
                               current={this.state.pagination.currentPage}
                               showSizeChanger
                               showQuickJumper
                               showTotal={total => `共 ${total} 条`}
                               onChange={this.changePage}
                               style={{marginTop: '20px'}}
                           />
                           </>
            </div>
        );
    }
}

export default Order
