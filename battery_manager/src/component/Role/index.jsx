import React,{Component} from 'react'
import http from '../../utils/Api'
import {Button, Form, Input, message, Pagination, Table, Modal, Drawer, Select} from "antd";
const {Option} = Select;
const {TextArea}=Input
// const roleInfo = {
//     id: 0,
//     name: '',
//     description: '',
//     remark: '',
//     status: 0
// }
class Role extends Component {

    state = {
        roleList: [],
        queryParams: {
            currentPage: 1,
            pageSize: 10,
            name: ''
        },
        roleInfo: {
            id: 0,
            name: '',
            description: '',
            status: 0
        },
        pagination: {
            total: 0,
            currentPage: 1
        },
        visible: false
    }

    getRoleList=() => {
        http.getRequest('/sys/role/listPage',this.state.queryParams).then(({data}) => {
            if (data.data.length >= 0) {
                this.setState({
                    roleList: data.data,
                    pagination: {
                        total: data.total
                    }
                },() => {
                    message.success('数据信息获取成功').then(() => {})
                })
            }
        })
    }

    handleDelete=(id) => {
        Modal.confirm({
            title: '提示框',
            content: '确认删除该用户么?',
            okText: '确认',
            cancelText: '取消',
            onOk: () => {
                http.deleteRequest('/sys/role/delete/'+ id).then(({data})=> {
                    if (data.code === 200) {
                        message.success(data.message).then(()=> {
                            this.getRoleList()
                        })
                    }
                })
            }
        })
    }

    componentDidMount() {
        this.getRoleList()
    }

    editUser=(value) => {
        this.setState({
            roleInfo: {
                ...this.state.roleInfo,
                ...value
            }
        },() => {
            //执行添加
            http.postRequest_json('/sys/role/edit',this.state.roleInfo).then(({data})=> {
                if (data.code === 200) {
                    message.success(data.message).then(()=> {
                        this.setState({
                            visible: false
                        },() => {
                            this.getRoleList()
                        })

                    })
                }
            })
        })
    }

    edit=(item)=> {
        console.log('edit',item)
        this.setState({
            roleInfo: item,
            visible: true
        })
    }

    changeStatus=(status,item)=> {
        console.log('@@@@@@',item)
        Modal.confirm({
            title: '提示框',
            content: '确认更改该用户的状态么?',
            okText: '确认',
            cancelText: '取消',
            onOk: () => {
                status = status === 1 ? 2 : 1;
                http.postRequest_form('/sys/role/status',{id: item.id,status: status}).then(({data}) => {
                    if (data.code === 200) {
                        message.success(data.message).then(() => {
                            this.getRoleList()
                        })
                    }
                })
            }
        })

    }

    changePage=(page,pageSize) => {
        this.setState({
            params:{
                ...this.state.queryParams,
                pageSize:pageSize,
                currentPage:page
            },
            pagination: {
                ...this.state.pagination,
                currentPage: page
            }
        },()=>{
            this.getRoleList()
        })
    }

    render() {
        return (
            <div>
                <Form layout={"inline"} style={{marginBottom: '30px'}}>
                    <Form.Item>
                        <Input placeholder="角色名" name={'name'} onChange={(e)=> {
                            this.setState({
                                queryParams: {...this.state.queryParams,name: e.target.value},

                            })
                        }} />
                    </Form.Item>
                    <Form.Item>
                        <Button type="primary" onClick={this.getRoleList}>查询</Button>
                    </Form.Item>
                    <Form.Item>
                        <Button type="primary" onClick={()=>{this.setState({visible: true})}}>添加角色</Button>
                    </Form.Item>

                </Form>
                <Table scroll={{ y: 500 }} style={{height: '500px'}} dataSource={this.state.roleList}
                       columns={[
                           {title:"编号", dataIndex:"id"},
                           {title:"角色名称", dataIndex:"name"},
                           {title:"角色描述", dataIndex:"description"},
                           {title:"角色状态", dataIndex:"status", render: (status,item) => {
                               return <Button
                                   type={item.status === 1 ? 'primary' : 'danger'} onClick={() => this.changeStatus(status,item)}>{item.status === 1 ? '可用' : '禁用'}</Button>
                               }},
                           {title:"删除",render:(item)=>{
                                   return(
                                       <Button type={"danger"}
                                               onClick={()=>this.handleDelete(item.id)}>删除</Button>
                                   )
                               }},
                           {title:"编辑",render:(item)=>{
                                   return(
                                       <Button type={"primary"} onClick={()=> this.edit(item)}>编辑</Button>
                                   )
                               }},]}
                       pagination={false} />
                <>
                    <Pagination
                        total={this.state.pagination.total}
                        current={this.state.pagination.currentPage}
                        showSizeChanger
                        showQuickJumper
                        showTotal={total => `共 ${total} 条`}
                        onChange={this.changePage}
                        style={{marginTop: '20px'}}
                    />
                </>

                <Drawer
                    title={this.state.roleInfo.id === 0 ? '添加角色' : '编辑角色'}
                    placement={'top'}
                    closable={true}
                    maskClosable={true}
                    onClose={()=> {this.setState({visible: !this.state.visible})}}
                    visible={this.state.visible}
                    height={'400'}>
                    <Form
                        formLayout={'horizontal'}
                        labelCol={{span: 2}}
                        name="basic"
                        onFinish={this.editUser}
                        initialValues={this.state.roleInfo}>
                        <Form.Item
                            label="角色名"
                            name="name"
                            rules={[
                                {
                                    required: true,
                                    message: '请输入你的角色名',
                                },]}>
                            <Input size="large" placeholder="请输入角色名" />
                        </Form.Item>
                        <Form.Item
                            label="备注"
                            name="description"
                            rules={[
                                {required:true,message:"请添加备注"}
                            ]}>
                            <TextArea  placeholder="请添加备注"/>
                        </Form.Item>
                        <Form.Item
                            label="角色状态"
                            name="status"
                            rules={[
                                {required:true,message:"请选择角色状态"}
                            ]}>
                            <Select defaultValue={this.state.roleInfo.status} size="large">
                                <Option value={0}>所有状态</Option>
                                <Option value={1}>可用</Option>
                                <Option value={2}>禁用</Option>
                            </Select>
                        </Form.Item>
                        <Form.Item style={{textAlign: 'center'}}>
                            <Button type="primary" size={'large'} htmlType="submit">提交</Button>
                        </Form.Item>
                    </Form>
                </Drawer>
            </div>
        );
    }
}

export default Role;
