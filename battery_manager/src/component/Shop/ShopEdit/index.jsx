import React from "react"
import 'antd/dist/antd.css';
import {Space, Card, Input, Form, Select, Button, message, Upload} from 'antd'
import http from "../../../utils/Api";
import {withRouter} from "react-router-dom";
import {UploadOutlined} from "@ant-design/icons";
const {Option} = Select
const {TextArea}=Input
class ShopEdit extends React.Component{
    constructor(props) {
        super(props);
        this.state = {
            shopInfo: {
                ...this.props.location.state.shopInfo,
                picture: ''
            },
            shopList:[],
            pagination: {
                total: 0
            }
        }
    }
    getShopList=()=>{
        http.getRequest('/fro/shop/list').then(({data})=>{
            if (data.code === 200){
                this.setState({
                    shopList:data.data
                })
            }
        })
    }
    addShop=(value)=> {
        console.log(this.state)
        this.setState({
            shopInfo: {
                ...value,
                picture: this.state.shopInfo.picture,
                id: this.state.shopInfo.id,
            }
        },() => {
            console.log('$$$$$$',this.state.shopInfo)
            http.postRequest_json('/fro/shop/edit',this.state.shopInfo).then(({data}) => {
                console.log(data)
                console.log(this.props)
                if (data.code === 200) {
                    message.success(data.message,).then(()=> {
                        this.props.history.replace('/shop')
                    })
                }
            })
        })
    }

    componentDidMount() {
        this.getShopList();
    }

    render() {
        return(
            <div>

                <Space direction="vertical" style={{width:"100%"}}>
                    <Card title="添加设备信息">
                        <Form
                            onFinish={this.addShop}
                            name="basic"
                            labelCol={{
                                span: 2,
                            }}
                            wrapperCol={{
                                span: 22,
                            }}
                            initialValues={
                                {
                                    ...this.props.location.state.shopInfo
                                }
                            }
                        >
                            <Form.Item
                                label="店铺名称"
                                name="name"
                                rules={[
                                    {
                                        required: true,
                                        message: '请输入你的店铺名称',
                                    },]}>
                                <Input id="name" size="large" placeholder="请输入店铺名称"/>
                            </Form.Item>
                            <Form.Item
                                label="店铺备注"
                                name="remark"
                                rules={[
                                    {required:true,message:"请添加店铺备注"}
                                ]}>
                                <TextArea  placeholder="请添加店铺备注"/>
                            </Form.Item>
                            <Form.Item
                                label="店铺地址"
                                name="address"
                                rules={[
                                    {required:true,message:"请输入店铺地址"}
                                ]}>
                                <Input size="large" placeholder="请输入店铺地址"/>
                            </Form.Item>
                            <Form.Item
                                label="经度"
                                name="longitude"
                                rules={[
                                    {required:true,message:"请输入店铺经度"}
                                ]}>
                                <Input size="large" placeholder="请输入店铺经度"/>
                            </Form.Item>
                            <Form.Item
                                label="纬度"
                                name="latitude"
                                rules={[
                                    {required:true,message:"请输入店铺纬度"}
                                ]}>
                                <Input size="large" placeholder="请输入店铺纬度"/>
                            </Form.Item>
                            <Form.Item label="设备图片">
                                <Upload
                                    action="/api1/upload"
                                    listType="picture"
                                    onChange={(e) => {
                                        console.log('@@@@@',e)
                                        if (e.file.status === 'done') {
                                            if (e.file.response.code === 200) {
                                                //说明返回成功了
                                                this.setState({
                                                    shopInfo: {
                                                        ...this.state.shopInfo,
                                                        picture: e.file.response.data.fileName
                                                    }
                                                },() => {
                                                    console.log('@@@@@@@@@@@@',this.state)
                                                })
                                            }
                                        }
                                    }}
                                >
                                    <Button icon={<UploadOutlined />}>上传</Button>
                                </Upload>
                            </Form.Item>
                            <Form.Item label="店铺状态" name={"status"} rules={[{ required: true }]}>
                                <Select defaultValue={0} style={{ width: 100 }} onChange={(e) => {this.setState({
                                    params: {
                                        ...this.state.params,
                                        status: e
                                    }
                                })}}>
                                    <Option value={0}>所有状态</Option>
                                    <Option value={1}>可用状态</Option>
                                    <Option value={2}>禁用状态</Option>
                                </Select>
                            </Form.Item>
                            <Form.Item>
                                <Button type="primary" htmlType="submit" style={{marginRight:0}}>提交</Button>
                            </Form.Item>
                        </Form>
                    </Card>
                </Space>
            </div>
        )
    }
}
export default withRouter(ShopEdit)
