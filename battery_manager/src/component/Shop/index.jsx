import React from 'react'

import {
    Button,
    Pagination,
    Table,
    message,
    Input,
    Modal,
    Select,
} from 'antd';

import {ExclamationCircleOutlined} from "@ant-design/icons";
import {Option} from "antd/es/mentions";

import baseUrl from "../../utils/baseUrl";

import http from '../../utils/Api'
import {NavLink} from "react-router-dom";

const params={
    name:'',
    status:0,
    pageSize:10,
    currentPage:1
}

export default class Shop extends React.Component {

    state={
        ShopList:[],
        pagination: {
            total: 0,
            currentPage: 1
        },
        params: {...params}
    }
    getShopList=()=>{
        http.getRequest('/fro/shop/listPage',this.state.params).then(({data})=>{
            if (data.data.length >= 0) {
                message.success({content: '数据获取成功'}).then(r => {})
                this.setState({
                    ShopList:data.data,
                    pagination:{
                        ...this.state.pagination,
                        total:data.total
                    }
                })
            }
        })
    }
    /* 删除按钮，点击弹出 */
    showDeleteConfirm = (id) => {
        Modal.confirm({
            title: "你确定要删除吗?",
            icon: <ExclamationCircleOutlined/>,
            okText: "确定",
            okType: "danger",
            cancelText: "取消",
            onOk: () => {
                this.deleteShop(id)
            }
        });
    };
    /* 删除按钮的方法，被onOk调用 */
    deleteShop = (id) => {
        http.deleteRequest(`/fro/shop/delete/${id}`).then(({data}) => {
            if (data.code===200) {
                console.log(data);
                this.getShopList()
            }
        })
        console.log(id);
    }
    /* 状态更改方法 */
    UpdateDisabled = (status,item) => {
        http.postRequest_form('/fro/shop/status',{status,id: item.id}).then(({data})=> {
            console.log(data)
            if (data.code === 200) {
                message.success(data.message)
                this.getShopList()
            }

        })
        console.log(item);

    }
    /* 状态更改的弹窗 */
    showUpdateConfirm = (status,item) => {
        Modal.confirm({
            title: "你确定要修改吗?",
            icon: <ExclamationCircleOutlined/>,
            okText: "确定",
            okType: "danger",
            cancelText: "取消",
            onOk: () => {
                status = status === 1 ? 2 : 1
                this.UpdateDisabled(status,item)
            }
        });
    }
    /* 通过状态查询设备 */
    searchShopByStatus = () => {
        this.getShopList()
    }
    statusChange=(e)=>{
        this.setState({
            params:{
                ...this.state.params,
                status:e
            }
        })
    }
    nameChange=(e)=>{
        this.setState({
            params:{
                ...this.state.params,
                name:e.target.value
            }
        })
    }
    /*更改一页显示条数*/
    changePage=(page,pageSize)=>{
        console.log(page,pageSize)
        this.setState({
            params:{
                ...this.state.params,
                pageSize:pageSize,
                currentPage:page
            },
            pagination: {
                ...this.state.pagination,
                currentPage: page
            }
        },()=>{
            this.getShopList()
        })

    }
    componentDidMount=()=> {
        this.getShopList()
    }

    render() {
        const columns=[
            { title:"店铺ID", dataIndex:"id"},
            { title:"店铺名称", dataIndex:"name"},
            { title:"店铺备注", dataIndex:"remark"},
            { title:"店铺地址", dataIndex:"address"},
            { title:"设备放置创建时间", dataIndex:"createTime"},
            { title:"设备更新时间", dataIndex:"updateTime"},
            { title:"经度", dataIndex:"longitude"},
            { title:"纬度", dataIndex:"latitude"},
            { title:"店铺照片", dataIndex:"picture",render: (picture) => <img src={baseUrl+'/getImgStream?imgPath='+ picture} alt="" width="60px"/>},
            {title: "店铺状态",dataIndex: "status", render: (status,item) => <Button type={item.status === 1 ? 'primary' : 'danger'} onClick={() => this.showUpdateConfirm(status,item)}>{item.status === 1 ? '可用' : '禁用'}</Button>},
            {title: "操作",key:"opt",width: '200px', render:(row)=>{
                    return(
                        <>
                            <Button type={"primary"}
                                    onClick={()=>this.props.history.push('/shop/edit',{shopInfo: row})}>编辑</Button>
                            &nbsp;
                            <Button type={"danger"} onClick={()=>{this.showDeleteConfirm(row.id)}}>删除</Button>
                        </>
                    )
                }}
        ]
        return (
            <div>
                <Input size="large" placeholder="查询店铺" name={"name"} allowClear style={{width:500,marginRight:20}} onChange={this.nameChange}/>
                <Select dropdownClassName={'statusMessage'} defaultValue={this.state.params.status} style={{ width: 120,marginRight:20 }} onChange={this.statusChange} size="large" name={"status"}>
                    <Option value={0}>所有状态</Option>
                    <Option value={1}>可用</Option>
                    <Option value={2}>禁用</Option>
                </Select>
                <Button size="large" type={"primary"} style={{marginRight:20}} onClick={this.searchShopByStatus} >查询</Button>
                <Button type="primary" size="large"><NavLink to={{pathname:'/shop/edit',state: {shopInfo: {id: 0}}}}>添加店铺</NavLink></Button>
                <p/>
                <Table scroll={{ y: 500 }} className={"table"} dataSource={this.state.ShopList} columns={columns} onChange={this.getShopList} pagination={false}/>
                <p/>
                <Pagination
                    showSizeChanger
                    defaultCurrent={params.currentPage}
                    defaultPageSize={params.pageSize}
                    total={this.state.pagination.total}
                    onChange={this.changePage}
                    showTotal={total => `共${total} 条数据`}
                />
            </div>
        );
    }
}
