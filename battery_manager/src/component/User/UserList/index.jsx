import React,{Component} from 'react'
import http from "../../../utils/Api";
import { Table,Input,Button, Modal, Pagination,Form,Select,message} from 'antd';
import {NavLink} from "react-router-dom";
import baseUrl from "../../../utils/baseUrl";
const { Option } = Select;
const { confirm } = Modal;

export default class UserList extends Component {
    state = {
        userList: [],
        params: {
            pageSize: 10,
            currentPage: 1,
            status: '',
            username:'',
            phone:''
        },
        pagination: {
            total: 0,
            currentPage: 1
        }
    }
    getUserList=() => {
        const {params} = this.state;
        http.getRequest('/sys/user/list', params).then(({data})=> {
            this.setState({
                userList:data.data,
                pagination: {
                    ...this.state.pagination,
                    total: data.total
                }
            })
        })
    }

    changeStatus=(status,item)=> {
        const obj = {
            id: item.id,
            status: status === 1 ? 2 : 1
        }
        Modal.confirm({
            title: '提示框',
            content: '确认更改该用户的状态么?',
            okText: '确认',
            cancelText: '取消',
            onOk: () => {
                http.postRequest_form('/sys/user/status', obj).then(({data})=> {
                    if (data.code === 200) {
                        this.getUserList()
                    }
                })
            }
        })
    }

    componentDidMount() {
        this.getUserList()
    }

    /*更改一页显示条数*/
    changePage=(page,pageSize)=>{
        this.setState({
            params:{
                ...this.state.params,
                pageSize:pageSize,
                currentPage:page
            },
            pagination: {
                ...this.state.pagination,
                currentPage: page
            }
        },()=>{
            this.getUserList()
        })

    }

    changeParams=(e) => {
        this.setState({
            params: {
                ...this.state.params,
                [e.target.name]: e.target.value
            }
        })
    }

   /*删除信息*/
    handleDelete(Id){
        confirm({
            title:"你确定删除该用户信息吗？",
            content: "你确定删除该用户信息吗？",
            onOk:()=> {
                http.deleteRequest(`/sys/user/delete/${Id}`).then(({data})=>{
                    if (data.code===200){
                        message.success(data.message).then(() => {
                            this.getUserList()
                        })

                    }
                })
            },
            onCancel() {
                console.log('Cancel');
            },
        })

    }

    render() {
        return (
            <div>
                {/*<img src="http://localhost:8250/getImgStream?imgPath=2F88E8AD9CB248F5AA76424FDC202308.png" alt=""/>*/}
                <Form layout={"inline"} style={{marginBottom: '30px'}}>
                    <Form.Item>
                        <Input placeholder="请输入用户名" name={'username'} onChange={this.changeParams} />
                    </Form.Item>
                    <Form.Item>
                        <Input placeholder="请输入手机号" name={'phone'} onChange={this.changeParams} />
                    </Form.Item>
                    <Form.Item>
                        <Select defaultValue={0} style={{ width: 100 }} onChange={(e) => {this.setState({
                            params: {
                                ...this.state.params,
                                status: e
                            }
                        })}}>
                            <Option value={0}>所有状态</Option>
                            <Option value={1}>可用状态</Option>
                            <Option value={2}>禁用状态</Option>
                        </Select>
                    </Form.Item>
                    <Form.Item>
                        <Button type="primary" onClick={()=> {this.getUserList()}}>查询</Button>
                    </Form.Item>
                    <Form.Item>
                        <Button type="primary"><NavLink to={{pathname:'/user/edit',state: {userInfo: {id: 0}}}}>添加用户</NavLink></Button>
                    </Form.Item>

                </Form>
                <Table scroll={{ y: 500 }} dataSource={this.state.userList}
                       columns={[
                           {title:"编号", dataIndex:"id"},
                           {title:"用户名", dataIndex:"username"},
                           {title:"昵称", dataIndex:"nickname"},
                           {title:"手机号", dataIndex:"phone"},
                           {title:"头像", dataIndex:"avatar", render: (avatar) => {
                                   return (
                                       <img src={baseUrl + '/getImgStream?imgPath='+ avatar} alt="" width="60px"/>
                                   )
                               }},
                           {title:"角色名称", dataIndex:"role",render: (role) => {
                                   return role.name;
                               }},
                           {title:"用户备注", dataIndex:"remark"},
                           {title: "用户状态", dataIndex: 'status',
                               render: (status,item) => {
                                   return (
                                       <Button
                                           type={item.status === 1 ? 'primary' : 'danger'} onClick={() => this.changeStatus(status,item)}>{item.status === 1 ? '可用' : '禁用'}</Button>
                                   )
                               }},
                           {title:"删除",render:(item)=>{
                               return(
                                   <Button type={"danger"}
                                           onClick={()=>this.handleDelete(item.id)}>删除</Button>
                           )
                       }},
                           {title:"编辑",render:(item)=>{
                                   return(
                                       <Button type={"primary"}
                                               onClick={()=>this.props.history.push('/user/edit',{userInfo: item})}>编辑</Button>
                                   )
                               }},
                       ]}
                       pagination={false} />
                <>
                    <Pagination
                        total={this.state.pagination.total}
                        current={this.state.pagination.currentPage}
                        showSizeChanger
                        showQuickJumper
                        showTotal={total => `共 ${total} 条`}
                        onChange={this.changePage}
                        style={{marginTop: '20px'}}
                    />
                </>
            </div>
        );
    }
}
