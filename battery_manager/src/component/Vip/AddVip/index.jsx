import React from "react"
import 'antd/dist/antd.css';
import {Space, Card, Input, Form, Button, message, Upload} from 'antd'
import http from "../../../utils/Api";
import {withRouter} from "react-router-dom";
import {UploadOutlined} from "@ant-design/icons";
const {TextArea}=Input
class AddUser extends React.Component{
    constructor(props) {
        super(props);
        this.state = {
            vipInfo: {
                ...this.props.location.state.vipInfo,
                avatar: ''
            },
            roleList:[],
            pagination: {
                total: 0
            }
        }
    }

    getRoleList() {
        http.getRequest('/sys/vip/list').then(({data}) => {
            if (data.code === 200) {
                this.setState({
                    roleList: data.data
                })
            }
        })
    }

    addVip=(value)=> {
        console.log('%%%%%%%%%%%%',value)
        this.setState({
            vipInfo: {
                ...value,
                id: this.state.vipInfo.id,
                avatar: this.state.vipInfo.avatar
            }
        },() => {
            http.postRequest_json('/sys/vip/edit',this.state.vipInfo).then(({data}) => {
                console.log(data)
                console.log(this.props)
                if (data.code === 200) {
                    message.success(data.message,).then(()=> {
                        this.props.history.replace('/vip/list')
                    })
                }
            })
        })
    }

    componentDidMount() {
        this.getRoleList()
    }

    render() {
        return(
            <div>

                <Space direction="vertical" style={{width:"100%"}}>
                    <Card title="添加会员信息">
                        <Form
                            onFinish={this.addVip}
                            name="basic"
                            labelCol={{
                                span: 2,
                            }}
                            wrapperCol={{
                                span: 22,
                            }}
                            initialValues={
                                {
                                    ...this.props.location.state.vipInfo
                                }
                            }
                        >
                            <Form.Item
                                label="用户名"
                                name="username"
                                rules={[
                                    {
                                        required: true,
                                        message: '请输入你的用户名',
                                    },]}>
                                <Input id="username" size="large" placeholder="请输入用户名"/>
                            </Form.Item>
                            {
                                this.props.location.state.vipInfo.id === 0 ? <Form.Item
                                    label="密码"
                                    name="password"
                                    rules={[
                                        {
                                            required: true,
                                            message: '请输入你的密码',
                                        },
                                    ]}
                                >
                                    <Input id="password" size="large" placeholder="请输入密码"/>
                                </Form.Item> : <></>
                            }

                            <Form.Item
                                label="邮箱"
                                name="email"
                                rules={[
                                    {required:true,message:"请设置你的邮箱"}
                                ]}>
                                <Input name="邮箱" size="large"  placeholder="邮箱"/>
                            </Form.Item>
                            <Form.Item
                                label="手机号"
                                name="phone"
                                rules={[
                                    {required:true,message:"请输入你的手机号"}
                                ]}>
                                <Input size="large" placeholder="请输入手机号"/>
                            </Form.Item>
                            <Form.Item
                                label="头像"
                                // rules={[{ required: true, message: '请选择头像' }]}
                            >
                                <Upload
                                    action="/api1/upload"
                                    listType="picture"
                                    onChange={(e) => {
                                        console.log('@@@@@',e)
                                        if (e.file.status === 'done') {
                                            if (e.file.response.code === 200) {
                                                //说明返回成功了
                                                this.setState({
                                                    vipInfo: {
                                                        ...this.state.vipInfo,
                                                        avatar: e.file.response.data.fileName
                                                    }
                                                },() => {})
                                            }
                                        }
                                    }}
                                >
                                    <Button icon={<UploadOutlined />}>上传</Button>
                                </Upload>
                            </Form.Item>
                            <Form.Item
                                label="余额"
                                name="balance"
                                rules={[{ required: true, message: '请输入用户余额' }]}
                            >
                                <Input />
                            </Form.Item>
                            <Form.Item
                                label="备注"
                                name="remark"
                                rules={[
                                    {required:true,message:"请添加备注"}
                                ]}>
                                <TextArea  placeholder="请添加备注"/>
                            </Form.Item>
                            <Form.Item
                                label="状态"
                                name="status"
                                rules={[{ required: true, message: '请输入用户状态' }]}
                            >
                                <Input />
                            </Form.Item>
                            <Form.Item >
                                <Button type="primary" htmlType="submit" style={{marginRight:0}}>提交</Button>
                            </Form.Item>
                        </Form>
                    </Card>
                </Space>
            </div>
        )
    }
}
export default withRouter(AddUser)
