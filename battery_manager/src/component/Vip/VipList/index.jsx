import React from "react";
import {Table, Button, Input, Modal, Form, message, Pagination, Select} from "antd";
import './index.module.css'
import http from '../../../utils/Api.js'
import {ExclamationCircleOutlined} from "@ant-design/icons";
import {NavLink} from "react-router-dom";
import baseUrl from "../../../utils/baseUrl";
const {Option} = Select

export default class VipList extends React.Component{
    constructor(props) {
        super(props);
        this.state={
            vipList:[],
            params: {
                pageSize:10,
                currentPage: 1,
                status: 0,
                name:'',
                phone:''
            },
            pagination:{
                total:0,
                currentPage:1
            }
        }
    }

    /*获取会员列表数据*/
    getVipList=() => {
        const {params} = this.state;
        console.log('$',this.state)
        http.getRequest('/sys/vip/list',params).then(({data})=>{
            this.setState({
                vipList:data.data,
                pagination:{
                    ...this.state.pagination,
                    total:data.total,
                }

            })
        })
    }
    componentDidMount() {
        this.getVipList()
    }


    /* 删除按钮，点击弹出 */
    showDeleteConfirm = (id) => {
        Modal.confirm({
            title: "你确定要删除吗?",
            icon: <ExclamationCircleOutlined/>,
            okText: "确定",
            okType: "danger",
            cancelText: "取消",
            onOk: () => {
                this.deleteVip(id)
            }
        });
    };


    /*根据ID删除会员信息*/
    deleteVip = (Id) =>{
        http.deleteRequest(`/sys/vip/delete/${Id}`).then(({data})=>{
            if (data.code===200){
                console.log(data)
                this.getVipList()
            }
        })
    }

    /*修改会员状态*/
    UpdateDisabled = (status,item) => {
        http.postRequest_form('/sys/vip/status',{status,id: item.id}).then(({data})=> {
            console.log(data)
            if (data.code === 200) {
                message.success(data.message)
                this.getVipList()
            }
        })
        console.log(item);
    }
    /* 状态更改的弹窗 */
    showUpdateConfirm = (status,item) => {
        Modal.confirm({
            title: "你确定要修改吗?",
            icon: <ExclamationCircleOutlined/>,
            okText: "确定",
            okType: "danger",
            cancelText: "取消",
            onOk: () => {
                status = status === 1 ? 2 : 1
                this.UpdateDisabled(status,item)
            }
        });
    }

    /*更改一页显示条数*/
    changePage=(page,pageSize)=>{
        console.log(page,pageSize)
        this.setState({
            params:{
                ...this.state.params,
                pageSize:pageSize,
                currentPage:page
            },
            pagination: {
                ...this.state.pagination,
                currentPage: page
            }
        },()=>{
            this.getVipList()
        })

    }

    changeParams=(e) => {
        console.log(this.state.params)
        this.setState({
            params: {
                ...this.state.params,
                [e.target.name]: e.target.value
            }
        })

    }

    render() {
        return (
            <div>
                <Form layout={"inline"} style={{marginBottom: '30px'}}>
                    <Form.Item>
                        <Input placeholder="请输入用户名" name={'name'} onChange={this.changeParams} />
                    </Form.Item>
                    <Form.Item>
                        <Input placeholder="请输入手机号" name={'phone'} onChange={this.changeParams} />
                    </Form.Item>
                    <Form.Item>
                        <Select defaultValue={0} style={{ width: 100 }} onChange={(e) => {this.setState({
                            params: {
                                ...this.state.params,
                                status: e
                            }
                        })}}>
                            <Option value={0}>所有状态</Option>
                            <Option value={1}>可用状态</Option>
                            <Option value={2}>禁用状态</Option>
                        </Select>
                    </Form.Item>
                    <Form.Item>
                        <Button type="primary" onClick={()=> {this.getVipList()}}>查询</Button>
                    </Form.Item>
                    <Form.Item>
                        <Button type="primary"><NavLink to={{pathname:'/vip/edit',state: {vipInfo: {id: 0}}}}>添加会员</NavLink></Button>
                    </Form.Item>
                </Form>
                <Table scroll={{ y: 500 }} dataSource={this.state.vipList} columns={[
                    {title:'编号',dataIndex:'id',key:'id'},
                    {title:'用户名',dataIndex:'username',key:'id'},
                    {title:'邮箱',dataIndex:'email',key:'id'},
                    {title:'联系方式',dataIndex:'phone',key:'id'},
                    {title:"头像", dataIndex:"avatar", render: (avatar) => {
                            return (
                                <img src={baseUrl + '/getImgStream?imgPath='+ avatar} alt="" width="60px"/>
                            )
                        }},
                    {title:'创建时间',dataIndex:'createTime',key:"id"},
                    {title:'更新时间',dataIndex:'updateTime',key:'id'},
                    {title:'余额',dataIndex:'balance',key:'id'},
                    {title:'备注',dataIndex:'remark',key:'id'},
                    {title: "设备状态",dataIndex: "status",
                        render: (status,item) => <Button type={item.status === 1 ? 'primary' : 'danger'} onClick={() => this.showUpdateConfirm(status,item)}>{item.status === 1 ? '可用' : '禁用'}</Button>},
                    {
                        title:"操作",key:"opt",render:(item)=>{
                            return (
                                <>
                                    <Button type="primary" ><NavLink to={{pathname:'/vip/edit',state: {vipInfo: item}}}>编辑</NavLink></Button>
                                    &nbsp;
                                    <Button type="danger" onClick={()=>this.showDeleteConfirm(item.id)}>删除</Button>
                                </>
                            )
                        }
                    }
                ]}  pagination={false}/>
                <>
                    <Pagination
                        total={this.state.pagination.total}
                        current={this.state.pagination.currentPage}
                        showSizeChanger
                        showQuickJumper
                        showTotal={total => `共 ${total} 条`}
                        onChange={this.changePage}
                        style={{marginTop: '20px'}}
                    />
                </>
            </div>
        );
    }
}

