import Mock from 'mockjs';
// export const login=Mock.mock('/login','post',{"code|0-1":0,name:Random.cname()})
const login=Mock.mock('/login','post',({body})=>{
    body=JSON.parse(body)
    if (body.username==="admin"&&body.password==="admin"){
        return {
            code: 1
        }
    }else {
        return {
            code: 0
        }
    }
})

const userList=Mock.mock('/user/list','get',{
    code:1,
    "data|3-15":[{
        "id|+1":1,
        "name":"@cname",
        "sex":"@character('男女')",
        "username":"@email(jifeng.com)",
        "status":"@natural(0,1)",
        "age":"@integer(15,50)",
        "city":"@county(true)"
    }],
    total:"@integer(200,500)"
})

const barryList=Mock.mock('/barry/list','get',{
    code:1,
    "data|3-15":[{
        "id|+1":1,
        "status":"@natural(0,1)"
    }],
    total:"@integer(200,500)"
})

const echartsData = Mock.mock('/echarts/line','get', {
    code: 1,
    data: {
        xList: ['一月', '三月', '五月', '七月', '九月'],
        yList: [{
            name: 'js',
            data: ['@integer(1,30)', '@integer(1,30)', '@integer(1,30)', '@integer(1,30)', '@integer(1,30)']
        },{
            name: 'jq',
            data: ['@integer(1,30)', '@integer(1,30)', '@integer(1,30)', '@integer(1,30)', '@integer(1,30)']
        },{
            name: 'vue',
            data: ['@integer(1,30)', '@integer(1,30)', '@integer(1,30)', '@integer(1,30)', '@integer(1,30)']
        },{
            name: 'react',
            data: ['@integer(1,30)', '@integer(1,30)', '@integer(1,30)', '@integer(1,30)', '@integer(1,30)']
        }]
    }
})
const pieData=Mock.mock('/echarts/pie','get',{
    code:1,
    data:{
        pieList:[
            {
                text:"王者荣耀",
                hours:5
            },{
                text:"皇室战争",
                hours:7
            },{
                text:"和平精英",
                hours: 3.5
            },{
                text:"英雄联盟",
                hours: 4.5
            },{
                text:"梦幻西游",
                hours: 4
            }
        ]
    }
})

export {login,userList,barryList,echartsData,pieData}
