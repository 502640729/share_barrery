import axios from "axios";
import qs from 'qs'

// axios.defaults.headers['Content-Type'] = 'application/x-www-form-urlencoded'; // 设置默认的请求头的Content-Type

axios.interceptors.request.use(config => {
    if (sessionStorage.getItem('tokenStr')) {
        config.headers['Authorization'] = sessionStorage.getItem('tokenStr')
    }

    config.baseURL = '/api1'
    config.headers['Authorization'] = 'eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJhZG1pbiIsImNyZWF0ZWQiOjE2Mjk2MzkyNDU3MDYsImV4cCI6MTYzMDI0NDA0NX0.1-4-q0cjiMe8GW40JD35pYW-SisliWN-iUqAAinSRLU'
    // 如果存在token,请求就会携带这个token
    return config
}, error => {
    console.log(error)
})

/**
 * @description 封装的get请求的方法
 * @param {*} url 请求的地址
 * @param {*} data  请求的数据
 * @returns 数据请求的promise对象
 */
function getRequest(url, data) {
    return axios.get(url, {
        params: data
    })
}

/**
 * @description 封装post请求的方法，传参json类型数据
 * @param {*} url 请求的地址
 * @param {*} data 请求的数据
 * @returns 数据请求的promise对象
 */
function postRequest_json(url, data) {
    return axios.post(url,data)
}

/**
 * @description 封装post请求的方法， 传参form表单类型数据
 * @param {*} url 请求的地址
 * @param {*} data 请求的数据
 * @returns 数据请求的promise对象
 */
function postRequest_form(url,data) {
    return axios({
        method: 'post',
        url,
        data: qs.stringify(data)
    })
}

/**
 * @description 封装delete请求的方法
 * @param {*} url 请求的地址
 * @param {*} data 请求的数据
 * @returns 数据请求的promise对象
 */
function deleteRequest(url, data) {
    return axios.delete(url, data)
}
const http = {
    getRequest,
    postRequest_json,
    postRequest_form,
    deleteRequest
}
export default http

