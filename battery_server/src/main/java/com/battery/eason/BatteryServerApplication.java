package com.battery.eason;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@MapperScan("com.battery.eason.mapper")
public class BatteryServerApplication {

    public static void main(String[] args) {
        SpringApplication.run(BatteryServerApplication.class, args);
    }

}
