package com.battery.eason.config;

import com.battery.eason.interceptor.UserInterceptor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * 拦截器配置
 *
 * @author HuangSiYuan
 * @since 2021-08-16
 */
@Configuration
public class InterceptorConfig implements WebMvcConfigurer {

    @Bean
    public UserInterceptor getUserInterceptor() {
        return new UserInterceptor();
    }

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        //要拦截的路径,拦截user下的所有请求,必须要用户登录才能访问,但是有一些不需要用户登录也可访问
        String[] addPathPatterns = {
                "/**"
        };
        //排除的路径
        String[] excludePathPatterns = {
                "/doc.html",
                "/webjars/**",
                "/swagger-resources",
                "/swagger-resources/**",
                "/captcha",
                "/sys/user/login",
                "/sys/user/list",
                "/getImgStream",
                "/upload"
        };

        //相当于mvc:interceptor    bean    class=""
        //要拦截的路径和排除的路径
        registry.addInterceptor(getUserInterceptor()).addPathPatterns(addPathPatterns).excludePathPatterns(excludePathPatterns);
    }

    @Override

    public void addCorsMappings(CorsRegistry registry) {
        registry.addMapping("/**")
                .allowedHeaders("*")
                .allowedMethods("*")
                .allowedOrigins("*");
    }
}
