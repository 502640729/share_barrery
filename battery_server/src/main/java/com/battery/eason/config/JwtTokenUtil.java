package com.battery.eason.config;

import com.battery.eason.pojo.FroUser;
import com.battery.eason.pojo.SysUser;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * JwtToken工具类
 *
 * @author HuangSiYuan
 * @since 2021-06-06
 */
@Component
public class JwtTokenUtil {

    /*初始化JWT荷载请求用户名*/
    private static final String CLAIM_KEY_USERNAME = "sub";
    /*初始化JWT创建时间*/
    private static final String CLAIM_KEY_CREATED = "created";

    /*配置文件里面的密钥*/
    @Value("${jwt.secret}")
    private String secret;

    /*配置文件里面的失效时间*/
    @Value("${jwt.expiration}")
    private Long expiration;

    /**
     * 根据用户信息生成token
     * @param sysUser 用户信息
     * @return  返回一个荷载用户
     */
    public String generateToken(SysUser sysUser) {
        /*准备荷载信息*/
        Map<String,Object> claims = new HashMap<>();
        claims.put(CLAIM_KEY_USERNAME,sysUser.getUsername());
        claims.put(CLAIM_KEY_CREATED,new Date());
        return generateToken(claims);
    }

    /**
     * 根据用户信息生成token
     * @param froUser 用户信息
     * @return  返回一个荷载用户
     */
    public String generateTokenFroUser(FroUser froUser) {
        /*准备荷载信息*/
        Map<String,Object> claims = new HashMap<>();
        claims.put(CLAIM_KEY_USERNAME,froUser.getUsername());
        claims.put(CLAIM_KEY_CREATED,new Date());
        return generateToken(claims);
    }

    /**
     * 根据荷载生成JWT token
     * @param claims    荷载信息
     * @return  返回一个Token字符串
     */
    private String generateToken(Map<String,Object> claims) {
        String token = Jwts.builder()
                .addClaims(claims)
                .setExpiration(generateExpirationDate())
                .signWith(SignatureAlgorithm.HS256,secret)
                .compact();
        return token;
    }

    /**
     * 生成token失效时间
     * @return  返回token的失效时间
     */
    private Date generateExpirationDate() {
        long time = System.currentTimeMillis() + expiration * 1000;
        return new Date(time);
    }

    /**
     * 从token中获取登录用户名
     * @param token
     * @return
     */
    public String getUserNameFromToken(String token) {
        String username;
        try {
            Claims claims = getClaimFromToken(token);
            username = claims.getSubject();
        }catch (Exception e) {
            username = null;
        }

        return username;
    }

    /**
     * 从token中获取荷载
     * @param token
     * @return
     */
    private Claims getClaimFromToken(String token) {
        Claims claims = null;
        /*parser是JJWT解析token的一个方法*/
        claims = Jwts.parser()
                .setSigningKey(secret)
                .parseClaimsJws(token)
                .getBody();
        return claims;
    }

    /**
     * 判断token是否有效
     * @param token token信息
     * @param sysUser 用户信息
     * @return
     */
    public boolean validateToken(String token,SysUser sysUser) {
        String username = getUserNameFromToken(token);
        return username.equals(sysUser.getUsername()) && isTokenExpiration(token);
    }

    /**
     * 通过失效时间判断token是否失效
     * @param token
     * @return
     */
    private boolean isTokenExpiration(String token) {
        Date expireDate = getExpiredDateFromToken(token);
        return !expireDate.before(new Date());
    }

    /**
     * 从token中获取过期时间
     * @param token
     * @return
     */
    private Date getExpiredDateFromToken(String token) {
        Claims claims = getClaimFromToken(token);
        return claims.getExpiration();
    }

    /**
     * 判断token是否可以被刷新
     * @param token
     * @return
     */
    public boolean canRefresh(String token) {
        return isTokenExpiration(token);
    }

    /**
     * 刷新token
     * @param token
     * @return
     */
    public String refreshToken(String token) {
        Claims claims = getClaimFromToken(token);
        claims.put(CLAIM_KEY_CREATED,new Date());
        return generateToken(claims);
    }
}
