package com.battery.eason.controller;


import com.battery.eason.pojo.FroBattery;
import com.battery.eason.pojo.FroEquipment;
import com.battery.eason.pojo.RespBean;
import com.battery.eason.pojo.RespPageBean;
import com.battery.eason.service.IFroBatteryService;
import com.battery.eason.service.IFroEquipmentService;
import com.battery.eason.utils.DateTimeUtil;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import org.springframework.stereotype.Controller;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author HuangSiYuan
 * @since 2021-08-16
 */
@CrossOrigin
@RestController
@RequestMapping("/fro/battery")
public class FroBatteryController {

    @Autowired
    private IFroBatteryService froBatteryService;

    @ApiOperation("充电宝信息添加或修改")
    @PostMapping(value = "/edit")
    public RespBean edit(@RequestBody FroBattery froBattery) {
        System.out.println(froBattery.toString());
        if (null==froBattery.getId() || froBattery.getId() == 0) {
            //说明是添加操作
            froBattery.setCreateTime(DateTimeUtil.getSysTime());
            if (froBatteryService.save(froBattery)) {
                return RespBean.success("充电宝信息添加成功!");
            }
            return RespBean.error("充电宝信息添加失败!");
        }else {
            froBattery.setUpdateTime(DateTimeUtil.getSysTime());
            if (froBatteryService.updateById(froBattery)) {
                return RespBean.success("充电宝信息修改成功!");
            }
            return RespBean.error("充电宝信息修改失败");
        }
    }

    @ApiOperation("/充电宝信息列表")
    @GetMapping("/list")
    public RespPageBean list(@RequestParam(defaultValue = "1") Integer currentPage,
                             @RequestParam(defaultValue = "10") Integer pageSize,
                             Integer status, String equipmentName) {
        return  froBatteryService.pageList(currentPage,pageSize,status,equipmentName);
    }

    @ApiOperation("充电宝信息删除")
    @DeleteMapping("/delete/{id}")
    public RespBean delete(@PathVariable Integer id) {
        if (froBatteryService.removeById(id)) {
            return RespBean.success("充电宝信息删除成功!");
        }
        return RespBean.error("充电宝信息删除失败!");
    }

    @ApiOperation("充电宝状态修改")
    @PostMapping("/status")
    public RespBean status(Integer id,Integer status) {
        FroBattery battery = froBatteryService.getById(id);
        battery.setStatus(status);
        if (froBatteryService.updateById(battery)) {
            return RespBean.success("充电宝状态更改成功!");
        }
        return RespBean.error("设备状态更改失败!");
    }

    @ApiOperation("根据ID查询充电宝")
    @GetMapping("/{id}")
    public RespBean getById(@PathVariable Integer id) {
        FroBattery battery = froBatteryService.getById(id);
        if (null != battery) {
            return RespBean.success("充电宝查询成功", battery);
        }
        return RespBean.error("充电宝查询失败!");
    }
}