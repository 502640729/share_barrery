package com.battery.eason.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.battery.eason.pojo.FroBattery;
import com.battery.eason.pojo.FroEquipment;
import com.battery.eason.pojo.RespBean;
import com.battery.eason.pojo.RespPageBean;
import com.battery.eason.service.IFroBatteryService;
import com.battery.eason.service.IFroEquipmentService;
import com.battery.eason.service.IFroUserService;
import com.battery.eason.utils.DateTimeUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiOperation;
import io.swagger.models.auth.In;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import org.springframework.stereotype.Controller;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author HuangSiYuan
 * @since 2021-08-16
 */
@CrossOrigin
@RestController
@RequestMapping("/sys/equipment")
public class FroEquipmentController {

    @Autowired
    private IFroEquipmentService froEquipmentService;

    @Autowired
    private IFroBatteryService froBatteryService;

    @ApiOperation("设备信息添加或修改")
    @PostMapping("/edit")
    public RespBean edit(@RequestBody FroEquipment froEquipment) {
        if (null == froEquipment.getId() || froEquipment.getId() == 0) {
            //说明是添加操作
            froEquipment.setCreateTime(DateTimeUtil.getSysTime());
            if (froEquipmentService.save(froEquipment)) {
                return RespBean.success("设备信息添加成功!");
            }
            return RespBean.error("设备信息添加失败!");
        }else {
            froEquipment.setUpdateTime(DateTimeUtil.getSysTime());
            if (froEquipmentService.updateById(froEquipment)) {
                return RespBean.success("设备信息修改成功!");
            }
            return RespBean.error("设备信息修改失败");
        }
    }

    @ApiOperation("/设备信息列表")
    @GetMapping("/list")
    public RespPageBean list(@RequestParam(defaultValue = "1") Integer currentPage,
                             @RequestParam(defaultValue = "10") Integer pageSize,
                             String name, Integer status) {
        return  froEquipmentService.pageList(currentPage,pageSize,name,status);
    }

    @ApiOperation("设备信息删除")
    @DeleteMapping("/delete/{id}")
    public RespBean delete(@PathVariable Integer id) {
        if (froEquipmentService.removeById(id)) {
            froBatteryService.remove(new QueryWrapper<FroBattery>().eq("equipment_id",id));
            return RespBean.success("设备信息删除成功!");
        }
        return RespBean.error("设备信息删除失败!");
    }

    @ApiOperation("设备状态更改")
    @PostMapping("/status")
    public RespBean status(Integer id,Integer status) {
        FroEquipment byId = froEquipmentService.getById(id);
        byId.setStatus(status);
        if (froEquipmentService.updateById(byId)) {
            return RespBean.success("设备状态更改成功!");
        }
        return RespBean.error("设备状态更改失败!");
    }

    @ApiOperation("根据ID查询设备")
    @GetMapping("/{id}")
    public RespBean getById(@PathVariable Integer id) {
        FroEquipment byId = froEquipmentService.getById(id);
        if (null != byId) {
            return RespBean.success("设备查询成功", byId);
        }
        return RespBean.error("设备查询失败!");
    }

}
