package com.battery.eason.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.battery.eason.pojo.*;
import com.battery.eason.service.IFroBatteryService;
import com.battery.eason.service.IFroOrderService;
import com.battery.eason.service.IFroUserService;
import com.battery.eason.utils.DateTimeUtil;
import io.swagger.annotations.ApiOperation;
import io.swagger.models.auth.In;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import org.springframework.stereotype.Controller;

import java.math.BigDecimal;
import java.util.List;
import java.util.Locale;
import java.util.UUID;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author HuangSiYuan
 * @since 2021-08-16
 */
@CrossOrigin
@RestController
@RequestMapping("/fro/order")
public class FroOrderController {

    @Autowired
    private IFroOrderService froOrderService;

    @Autowired
    private IFroUserService froUserService;

    @Autowired
    private IFroBatteryService batteryService;

    @ApiOperation("分页查询所有订单信息")
    @GetMapping("/list")
    public RespPageBean listPage(@RequestParam(defaultValue = "1") Integer currentPage,
                                 @RequestParam(defaultValue = "10") Integer pageSize,
                                 String username, String name) {
        return froOrderService.listPage(currentPage,pageSize,username,name);
    }

    @ApiOperation("订单删除")
    @DeleteMapping("/delete/{id}")
    public RespBean delete(@PathVariable("id") Integer id) {
        FroOrder froOrder = froOrderService.getById(id);
        froOrder.setStatus(2);
        if (froOrderService.updateById(froOrder)) {
            return RespBean.success("订单删除成功!");
        }
        return RespBean.error("订单删除失败!");
    }

    @ApiOperation("根据ID查询订单信息")
    @GetMapping("/{id}")
    public RespBean getById(@PathVariable("id") Integer id) {
        FroOrder byId = froOrderService.getById(id);
        if (null != byId) {
            return RespBean.success("订单获取成功", byId);
        }
        return RespBean.error("订单获取失败!");
    }

    @ApiOperation("根据用户ID获取订单信息")
    @GetMapping("/user/{userId}")
    public RespBean getByUserId(@PathVariable("userId") Integer userId) {
        List<FroOrder> orders = froOrderService.getListByUserId(userId);
        if (null != orders) {
            return RespBean.success("订单获取成功!",orders);
        }
        return RespBean.error("订单获取失败!");
    }

    @ApiOperation("订单开始")
    @PostMapping("/add")
    public RespBean addOrder(@RequestBody FroOrder froOrder) {
        froOrder.setCreateTime(DateTimeUtil.getSysTime());
        froOrder.setStatus(1);
        froOrder.setOrderId(UUID.randomUUID().toString().toUpperCase(Locale.ROOT));
        //判断当前用户现在是否只有一个订单状态,如果有订单状态的话就提示不能充电
        FroOrder froOrder1 = froOrderService.getOne(new QueryWrapper<FroOrder>().eq("status",1).eq("user_id",froOrder.getUserId()));
        if (null == froOrder1) {
            //说明没有这个数据
            if (froOrderService.save(froOrder)) {
                return RespBean.success("订单成功生成!",froOrder);
            }
            return RespBean.error("订单生成失败!");
        }
        return RespBean.error("您目前已有一个订单在运行,不能在生成订单!");
    }

    @ApiOperation("订单结束")
    @PostMapping("/end")
    public RespBean endOrder(String userId,String orderId,String batteryId) {
        System.out.println(userId);
        System.out.println(orderId);
        System.out.println(batteryId);
        //修改订单
        FroOrder order = froOrderService.getOne(new QueryWrapper<FroOrder>().eq("order_id", orderId));

        //将订单修改为完成
        order.setStatus(3);
        order.setEndTime(DateTimeUtil.getSysTime());
        FroBattery froBattery = batteryService.getById(batteryId);
        FroUser froUser = froUserService.getById(userId);
        BigDecimal balance = new BigDecimal(froUser.getBalance());
        BigDecimal froCount = new BigDecimal(froBattery.getRent());
        String newBalance = balance.subtract(froCount).toString();
        froUser.setBalance(newBalance);
        order.setMoney(froCount.toString());
        froUserService.updateById(froUser);
        if (froOrderService.updateById(order)) {
            return RespBean.success("订单结束成功,该订单使用了: " + froCount + "元, 当前账户余额为: " + newBalance);
        }
        return RespBean.error("订单无法结束,请联系管理员!");
    }

    @ApiOperation("查询正在使用的订单")
    @GetMapping("/search/{id}")
    public RespBean search(@PathVariable("id") Integer id) {
        FroOrder one = froOrderService.getOne(new QueryWrapper<FroOrder>().eq("user_id", id).eq("status", 1));
        if (null == one) {
            //说明查询不到,那就查询为3的订单
            FroOrder one1 = froOrderService.getOne(new QueryWrapper<FroOrder>().eq("user_id", id).eq("status", 3).orderByDesc("id").last("limit 1"));
            return RespBean.success("订单获取成功", one1);
        }
        return RespBean.success("订单获取成功",one);
    }

}
