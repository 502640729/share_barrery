package com.battery.eason.controller;


import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author HuangSiYuan
 * @since 2021-08-16
 */
@CrossOrigin
@RestController
@RequestMapping("/fro/question")
public class FroQuestionController {

}
