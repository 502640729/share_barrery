package com.battery.eason.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.battery.eason.pojo.*;
import com.battery.eason.service.IFroBatteryService;
import com.battery.eason.service.IFroEquipmentService;
import com.battery.eason.service.IFroShopService;
import com.battery.eason.utils.DateTimeUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.models.auth.In;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import org.springframework.stereotype.Controller;

import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author HuangSiYuan
 * @since 2021-08-16
 */
@CrossOrigin
@RestController
@RequestMapping("/fro/shop")
public class FroShopController {

    @Autowired
    private IFroShopService froShopService;

    @Autowired
    private IFroEquipmentService froEquipmentService;

    @ApiOperation("获取所有店铺")
    @GetMapping("/list")
    public RespBean list() {
        return RespBean.success("数据获取成功",froShopService.list(new QueryWrapper<FroShop>().eq("status",1)));
    }

    @ApiOperation("分页获取所有店铺信息")
    @GetMapping("/listPage")
    public RespPageBean listPage(@RequestParam(defaultValue = "1") Integer currentPage,
                                 @RequestParam(defaultValue = "10") Integer pageSize,
                                 Integer status, String name, String address) {
        return froShopService.listPage(currentPage,pageSize,status,name,address);
    }

    @ApiOperation("删除店铺信息")
    @DeleteMapping("/delete/{id}")
    public RespBean delete(@PathVariable("id") Integer id){
        if (froShopService.removeById(id)) {
            return RespBean.success("店铺信息删除成功");
        }
        return RespBean.error("店铺信息删除失败!");
    }

    @ApiOperation("添加或修改店铺信息")
    @PostMapping("/edit")
    public RespBean edit(@RequestBody FroShop froShop) {
        if (null == froShop.getId() || froShop.getId() == 0) {
            //说明是添加
            froShop.setCreateTime(DateTimeUtil.getSysTime());
            if (froShopService.save(froShop)) {
                return RespBean.success("店铺信息添加成功!");
            }
            return RespBean.error("店铺信息添加失败!");
        }else {
            //说明是编辑
            froShop.setUpdateTime(DateTimeUtil.getSysTime());
            if (froShopService.updateById(froShop)) {
                return RespBean.success("店铺信息修改成功!");
            }
            return RespBean.error("店铺信息修改失败!");
        }
    }

    @ApiOperation("修改店铺状态")
    @PostMapping("/status")
    public RespBean status(Integer id,Integer status) {
        FroShop froShop = froShopService.getById(id);
        froShop.setStatus(status);
        froShop.setUpdateTime(DateTimeUtil.getSysTime());
        if (froShopService.updateById(froShop)) {
            return RespBean.success("店铺状态修改成功");
        }
        return RespBean.error("店铺状态修改失败!");
    }

    @ApiOperation("根据ID查询店铺信息")
    @GetMapping("/{id}")
    public RespBean getById(@PathVariable Integer id) {
        FroShop froShop = froShopService.getById(id);
        if (null != froShop) {
            return RespBean.success("店铺信息查询成功!",froShop);
        }
        return RespBean.error("店铺信息查询失败!");
    }

    @ApiOperation("获取店铺下关联的级联查询充电宝信息")
    @GetMapping("/equipment/{id}")
    public RespBean getByShopId(@PathVariable("id") Integer id) {
        //首先获取设备信息
        List<FroEquipment> froEquipments = froEquipmentService.getBatteryByShopId(id);
        if (froEquipments.size() > 0) {
            return RespBean.success("数据获取成功", froEquipments);
        }
        return RespBean.error("数据获取失败!");
    }

}
