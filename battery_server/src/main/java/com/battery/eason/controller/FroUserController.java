package com.battery.eason.controller;


import com.battery.eason.pojo.*;
import com.battery.eason.service.IFroUserService;
import com.battery.eason.utils.DateTimeUtil;
import com.battery.eason.utils.MD5Util;
import com.battery.eason.utils.SmsUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.models.auth.In;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import org.springframework.stereotype.Controller;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.awt.image.RescaleOp;
import java.math.BigDecimal;
import java.util.Map;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author HuangSiYuan
 * @since 2021-08-16
 */
@CrossOrigin
@RestController
@RequestMapping("/sys/vip")
public class FroUserController {

    @Autowired
    private IFroUserService froUserService;

    @ApiOperation("会员列表查询")
    @GetMapping("/list")
    public RespPageBean list(@RequestParam(defaultValue = "1") Integer currentPage,
                             @RequestParam(defaultValue = "10") Integer pageSize,
                             String name, String phone, Integer status) {
        return froUserService.pageList(currentPage,pageSize,name,phone,status);
    }

    @ApiOperation("会员用户添加或修改")
    @PostMapping("/edit")
    public RespBean add(@RequestBody FroUser froUser) {
        System.out.println(froUser.toString());
        if (null == froUser.getId() || froUser.getId() == 0) {
            froUser.setCreateTime(DateTimeUtil.getSysTime());
            froUser.setPassword(MD5Util.md5(froUser.getPassword()));
            //说明是添加操作
            if (froUserService.save(froUser)) {
                System.out.println("数据添加成功");
                return RespBean.success("会员数据添加成功!");
            }else {
                return RespBean.error("会员数据添加失败!");
            }
        }else {
            froUser.setUpdateTime(DateTimeUtil.getSysTime());
            if (froUserService.updateById(froUser)) {
                return RespBean.success("会员用户数据修改成功");
            }else {
                return RespBean.error("会员数据修改失败!");
            }
        }
    }

    @ApiOperation("会员数据删除")
    @DeleteMapping("/delete/{id}")
    public RespBean delete(@PathVariable Integer id) {
        if (froUserService.removeById(id)) {
            return RespBean.success("会员数据删除成功!");
        }
        return RespBean.error("会员数据删除失败!");
    }

    @ApiOperation("会员状态更改")
    @PostMapping("/status")
    public RespBean status(Integer id,Integer status) {
        System.out.println(id);
        System.out.println(status);
        FroUser froUser = froUserService.getById(id);
        froUser.setStatus(status);
        if (froUserService.updateById(froUser)) {
            return RespBean.success("会员状态更改成功!");
        }
        return RespBean.error("会员状态更改失败!");
    }

    @ApiOperation("前台用户短信接口")
    @PostMapping("/sms")
    public RespBean sendMessage(@RequestParam("phone") String phone,@RequestParam("uuid") String uuid, HttpServletRequest request) {
        if (null == phone) {
            return RespBean.error("手机号不能为空!");
        }
        String code = SmsUtil.randomCode();
        Map<String, Object> map = SmsUtil.sendMsg(phone, code);
        if ( (Integer) map.get("error_code") != 0) {
            //发送失败
            return RespBean.error("短信发送失败,请确定手机号是否正确!");
        }
        //发送消息成功
        //将验证码存储在request全局上下文作用域中
        request.getServletContext().setAttribute(uuid,code);
        return RespBean.success("短信发送成功!");
    }

    @ApiOperation("前台用户登录")
    @PostMapping("/login")
    public RespBean login(@RequestBody SysLoginParams loginParams, HttpServletRequest request) {
        return froUserService.login(loginParams,request);
    }

    @ApiOperation("前台用户头像上传")
    @PostMapping("/upload")
    public RespBean upload(MultipartFile file,Integer id) {
        return froUserService.uploadAvatar(file,id);
    }

    @ApiOperation("会员充值接口")
    @PostMapping("/balance")
    public RespBean balance(Integer id,String balance) {
        FroUser froUser = froUserService.getById(id);
        BigDecimal newAdd = new BigDecimal(balance);
        BigDecimal oldBalance = new BigDecimal(froUser.getBalance());
        String newBalance = newAdd.add(oldBalance).toString();
        //计算出新的余额
        froUser.setBalance(newBalance);
        //进行更新余额
        if (froUserService.updateById(froUser)) {
            return RespBean.success("充值成功!");
        }
        return RespBean.error("充值失败!");
    }

    @ApiOperation("根据会员ID获取会员信息")
    @GetMapping("/{id}")
    public RespBean getById(@PathVariable("id") Integer id) {
        FroUser froUser = froUserService.getById(id);
        if (null != froUser) {
            return RespBean.success("会员信息获取成功!",froUser);
        }
        return RespBean.error("会员信息获取失败!");
    }

}