package com.battery.eason.controller;


import com.battery.eason.pojo.RespBean;
import com.battery.eason.pojo.SysBasic;
import com.battery.eason.pojo.SysMessage;
import com.battery.eason.service.ISysBasicService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import org.springframework.stereotype.Controller;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author HuangSiYuan
 * @since 2021-08-16
 */
@CrossOrigin
@RestController
@RequestMapping("/sys/basic")
public class SysBasicController {

    @Autowired
    private ISysBasicService basicService;

    @ApiOperation("系统基本信息查询")
    @GetMapping("/list")
    public RespBean list() {
        return RespBean.success("消息获取成功!", basicService.list());
    }

    @ApiOperation("系统基本信息添加或修改")
    @PostMapping("/edit")
    public RespBean edit(@RequestBody SysBasic sysBasic) {
        if (null == sysBasic.getId() || sysBasic.getId() == 0) {
            if (basicService.save(sysBasic)) {
                return RespBean.success("系统基本信息添加成功!");
            }
            return RespBean.error("系统基本信息添加失败!");
        }else {
            if (basicService.updateById(sysBasic)) {
                return RespBean.success("系统基本信息修改成功!");
            }
            return RespBean.error("系统基本信息修改失败!");
        }
    }

    @ApiOperation("根据ID查询系统基本信息")
    @GetMapping("/{id}")
    public RespBean searchById(@PathVariable("id") Integer id) {
        SysBasic sysBasic =  basicService.getById(id);
        if (null != sysBasic) {
            return RespBean.success("系统基本信息获取成功", sysBasic);
        }
        return RespBean.error("系统基本信息获取失败!");
    }



}
