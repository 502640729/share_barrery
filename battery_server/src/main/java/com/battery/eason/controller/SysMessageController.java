package com.battery.eason.controller;


import com.battery.eason.pojo.RespBean;
import com.battery.eason.pojo.RespPageBean;
import com.battery.eason.pojo.SysMessage;
import com.battery.eason.service.ISysMessageService;
import com.battery.eason.utils.DateTimeUtil;
import io.swagger.annotations.ApiOperation;
import io.swagger.models.auth.In;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import org.springframework.stereotype.Controller;

import javax.xml.crypto.Data;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author HuangSiYuan
 * @since 2021-08-16
 */
@CrossOrigin
@RestController
@RequestMapping("/sys/message")
public class SysMessageController {
    @Autowired
    private ISysMessageService messageService;

    @ApiOperation("消息列表查询")
    @GetMapping("/list")
    public RespPageBean list(@RequestParam(defaultValue = "1") Integer currentPage,
                             @RequestParam(defaultValue = "10") Integer pageSize,
                             String title,String username) {
        return messageService.listPage(currentPage,pageSize,title,username);
    }

    @ApiOperation("消息添加或修改")
    @PostMapping("/edit")
    public RespBean edit(@RequestBody SysMessage sysMessage) {
        if (null == sysMessage.getId() || sysMessage.getId() == 0) {
            sysMessage.setCreateTime(DateTimeUtil.getSysTime());
            if (messageService.save(sysMessage)) {
                return RespBean.success("消息添加成功!");
            }
            return RespBean.error("消息添加失败!");
        }else {
            sysMessage.setUpdateTime(DateTimeUtil.getSysTime());
            if (messageService.updateById(sysMessage)) {
                return RespBean.success("消息修改成功!");
            }
            return RespBean.error("消息编辑失败!");
        }
    }

    @ApiOperation("消息删除")
    @DeleteMapping("/delete/{id}")
    public RespBean delete(@PathVariable("id") Integer id) {
        if (messageService.removeById(id)) {
            return RespBean.success("消息删除成功!");
        }
        return RespBean.error("消息删除失败!");
    }

    @ApiOperation("根据ID查询消息")
    @GetMapping("/{id}")
    public RespBean searchById(@PathVariable("id") Integer id) {
        SysMessage sysMessage = messageService.getById(id);
        if (null != sysMessage) {
            return RespBean.success("消息获取成功",sysMessage);
        }
        return RespBean.error("消息获取失败!");
    }

}
