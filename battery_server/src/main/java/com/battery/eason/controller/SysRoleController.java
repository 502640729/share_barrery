package com.battery.eason.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.battery.eason.pojo.RespBean;
import com.battery.eason.pojo.RespPageBean;
import com.battery.eason.pojo.SysRole;
import com.battery.eason.service.ISysRoleService;
import com.battery.eason.utils.DateTimeUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import org.springframework.stereotype.Controller;

/**
 * <p>
 *  系统角色管理控制器
 * </p>
 *
 * @author HuangSiYuan
 * @since 2021-08-16
 */
@CrossOrigin
@RestController
@Api("角色管理控制器")
@RequestMapping("/sys/role")
public class SysRoleController {

    @Autowired
    private ISysRoleService sysRoleService;

    @GetMapping("/list")
    @ApiOperation("获取所有角色信息")
    public RespBean list() {
        return RespBean.success("数据获取成功",sysRoleService.list(new QueryWrapper<SysRole>().eq("status",1)));
    }

    @ApiOperation("角色信息查询以及分页")
    @GetMapping("/listPage")
    public RespPageBean listPage(@RequestParam(defaultValue = "1") Integer currentPage,
                                 @RequestParam(defaultValue = "10") Integer pageSize,
                                 String name) {
        return sysRoleService.listPage(currentPage,pageSize,name);
    }

    @ApiOperation("角色删除")
    @DeleteMapping("/delete/{id}")
    public RespBean delete(@PathVariable("id") Integer id) {
        if (sysRoleService.removeById(id)) {
            return RespBean.success("角色信息删除成功!");
        }
        return RespBean.error("角色信息删除失败!");
    }

    @ApiOperation("角色添加")
    @PostMapping("/edit")
    public RespBean edit(@RequestBody SysRole sysRole) {
        if (null == sysRole.getId() ||sysRole.getId() == 0) {
            //添加角色操作
            sysRole.setCreateTime(DateTimeUtil.getSysTime());
            if (sysRoleService.save(sysRole)) {
                return RespBean.success("角色信息添加成功!");
            }
            return RespBean.error("角色信息添加失败!");
        }else {
            //更新角色操作
            sysRole.setUpdateTime(DateTimeUtil.getSysTime());
            if (sysRoleService.updateById(sysRole)) {
                return RespBean.success("角色信息修改成功!");
            }
            return RespBean.error("角色信息修改失败!");
        }
    }

    @ApiOperation("角色状态变更")
    @PostMapping("/status")
    public RespBean status(Integer id,Integer status) {
        System.out.println("111111111111: " + id);
        SysRole role = sysRoleService.getById(id);
        System.out.println(role.toString());
        role.setStatus(status);
        if (sysRoleService.updateById(role)) {
            return RespBean.success("角色状态更新成功!");
        }
        return RespBean.error("角色状态更新失败!");
    }

}
