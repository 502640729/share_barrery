package com.battery.eason.controller;


import com.battery.eason.pojo.RespBean;
import com.battery.eason.pojo.RespPageBean;
import com.battery.eason.pojo.SysLoginParams;
import com.battery.eason.pojo.SysUser;
import com.battery.eason.service.ISysUserService;
import com.battery.eason.utils.DateTimeUtil;
import com.battery.eason.utils.MD5Util;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.models.auth.In;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.Date;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author HuangSiYuan
 * @since 2021-08-16
 */
@CrossOrigin
@RestController
@RequestMapping("/sys/user")
public class SysUserController {

    @Autowired
    private ISysUserService sysUserService;

    @ApiOperation("系统用户登录")
    @PostMapping("/login")
    public RespBean SysLogin(@RequestBody SysLoginParams loginParams, HttpServletRequest request) {
        return sysUserService.login(loginParams,request);
    }

    @ApiOperation("系统用户添加或修改")
    @PostMapping("/edit")
    public RespBean add(@RequestBody SysUser sysUser) {
        System.out.println(sysUser.toString());
        if (null == sysUser.getId() || sysUser.getId() == 0) {
            sysUser.setCreateTime(DateTimeUtil.getSysTime());
            sysUser.setPassword(MD5Util.md5(sysUser.getPassword()));
            //说明是添加操作
            if (sysUserService.save(sysUser)) {
                return RespBean.success("系统用户数据添加成功!");
            }
        }else {
            sysUser.setUpdateTime(DateTimeUtil.getSysTime());
            if (sysUserService.updateById(sysUser)) {
                return RespBean.success("系统用户数据修改成功");
            }
        }
        return RespBean.error("系统用户数据编辑失败!");
    }

    @ApiOperation("系统用户密码修改")
    @PostMapping("/changePassword")
    public RespBean changePassword(Integer id,String oldPassword,String newPassword,HttpSession session) {
        SysUser sysUser = sysUserService.getById(id);
        //判断密码是否正确
        if (!sysUser.getPassword().equals(MD5Util.md5(oldPassword))) {
            return RespBean.error("原密码错误");
        }
        sysUser.setPassword(MD5Util.md5(newPassword));
        if (sysUserService.updateById(sysUser)) {
            //密码修改成功之后需要清除session信息
            session.removeAttribute(sysUser.getUsername());
            return RespBean.success("密码修改成功!");
        }
        return RespBean.error("密码修改失败!");
    }

    @ApiOperation("系统用户删除")
    @DeleteMapping("/delete/{id}")
    public RespBean delete(@PathVariable("id") Integer id) {
        if (sysUserService.removeById(id)) {
            return RespBean.success("系统用户删除成功!");
        }
        return RespBean.error("系统用户删除失败");
    }

    @ApiOperation("系统用户查询")
    @GetMapping("/list")
    public RespPageBean list(@RequestParam(defaultValue = "1") Integer currentPage,
                                  @RequestParam(defaultValue = "10") Integer pageSize,
                                  String username, String phone, Integer status) {
        return sysUserService.pageList(currentPage,pageSize,username,phone,status);
    }

    @ApiOperation("系统用户重名验证")
    @PostMapping("/checkUsername")
    public Boolean checkUsername(String username) {
        return null == sysUserService.getSysUserByUsername(username);
    }

    @ApiOperation("根据ID查询系统用户")
    @GetMapping("/{id}")
    public RespBean getUserById(@PathVariable Integer id) {
        SysUser sysUser = sysUserService.getById(id);
        if (null != sysUser) {
            return RespBean.success("用户信息获取成功", sysUser);
        }
        return RespBean.error("用户信息获取失败!");
    }

    @ApiOperation("系统用户状态更改")
    @PostMapping("/status")
    public RespBean status(Integer id,Integer status) {
        SysUser sysUser = sysUserService.getById(id);
        sysUser.setStatus(status);
        if (sysUserService.updateById(sysUser)) {
            return RespBean.success("用户状态更改成功!");
        }
        return RespBean.error("用户状态更改失败!");
    }
}