package com.battery.eason.mapper;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.battery.eason.pojo.FroEquipment;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author HuangSiYuan
 * @since 2021-08-16
 */
@Mapper
public interface FroEquipmentMapper extends BaseMapper<FroEquipment> {

    IPage<FroEquipment> getEquipmentByPage(Page<FroEquipment> equipmentPage, @Param("name") String name,@Param("status") Integer status);

    List<FroEquipment> getBattery(@Param("shopId") Integer shopId);

}
