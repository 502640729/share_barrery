package com.battery.eason.mapper;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.battery.eason.pojo.FroShop;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author HuangSiYuan
 * @since 2021-08-16
 */
@Mapper
public interface FroShopMapper extends BaseMapper<FroShop> {

    IPage<FroShop> listPage(Page<FroShop> page, @Param("status") Integer status,@Param("name") String name,@Param("address") String address);
}
