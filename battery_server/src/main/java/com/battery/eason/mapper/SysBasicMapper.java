package com.battery.eason.mapper;

import com.battery.eason.pojo.SysBasic;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author HuangSiYuan
 * @since 2021-08-16
 */
public interface SysBasicMapper extends BaseMapper<SysBasic> {

}
