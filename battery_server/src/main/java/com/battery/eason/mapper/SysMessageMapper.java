package com.battery.eason.mapper;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.battery.eason.pojo.SysMessage;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author HuangSiYuan
 * @since 2021-08-16
 */
@Mapper
public interface SysMessageMapper extends BaseMapper<SysMessage> {

    IPage<SysMessage> messageByPage(Page<SysMessage> page, @Param("title") String title,@Param("username") String username);
}
