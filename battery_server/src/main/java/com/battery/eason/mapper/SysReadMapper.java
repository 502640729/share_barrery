package com.battery.eason.mapper;

import com.battery.eason.pojo.SysRead;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author HuangSiYuan
 * @since 2021-08-16
 */
public interface SysReadMapper extends BaseMapper<SysRead> {

}
