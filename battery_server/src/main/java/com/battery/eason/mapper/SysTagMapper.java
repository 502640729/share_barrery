package com.battery.eason.mapper;

import com.battery.eason.pojo.SysTag;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author HuangSiYuan
 * @since 2021-08-16
 */
public interface SysTagMapper extends BaseMapper<SysTag> {

}
