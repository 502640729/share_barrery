package com.battery.eason.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 文件信息
 *
 * @author HuangSiYuan
 * @since 2021-08-21
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class FileInfo {

    private String fileName;

    private String path;

    private String createTime;

}
