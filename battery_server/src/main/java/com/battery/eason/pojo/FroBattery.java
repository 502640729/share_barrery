package com.battery.eason.pojo;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author HuangSiYuan
 * @since 2021-08-16
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("fro_battery")
@ApiModel(value="FroBattery对象", description="")
public class FroBattery implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "充电宝编号")
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "充电宝状态")
    private Integer status;

    @ApiModelProperty(value = "充电宝备注")
    private String remark;

    @ApiModelProperty(value = "充电宝创建时间")
    @TableField("create_time")
    private String createTime;

    @ApiModelProperty(value = "充电宝更新状态时间")
    @TableField("update_time")
    private String updateTime;

    @ApiModelProperty(value = "设备ID")
    @TableField("equipment_id")
    private Integer equipmentId;

    @ApiModelProperty(value = "充电宝租金，每十分钟收费一元")
    private String rent;

    @ApiModelProperty("设备信息")
    @TableField(exist = false)
    private FroEquipment froEquipment;


}
