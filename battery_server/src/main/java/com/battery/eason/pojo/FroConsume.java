package com.battery.eason.pojo;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author HuangSiYuan
 * @since 2021-08-16
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("fro_consume")
@ApiModel(value="FroConsume对象", description="")
public class FroConsume implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "消费表唯一ID")
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "消费人")
    @TableField("user_id")
    private Integer userId;

    @ApiModelProperty(value = "消费金额")
    private Double money;

    @ApiModelProperty(value = "消费时间")
    private String time;

    @ApiModelProperty(value = "消费类型")
    private Integer type;

    @ApiModelProperty(value = "消费状态")
    private Integer status;

    @ApiModelProperty(value = "消费备注")
    private String remark;


}
