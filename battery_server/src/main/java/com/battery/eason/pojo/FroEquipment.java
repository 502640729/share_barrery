package com.battery.eason.pojo;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import java.util.List;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author HuangSiYuan
 * @since 2021-08-16
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("fro_equipment")
@ApiModel(value="FroEquipment对象", description="")
public class FroEquipment implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "设备编号")
    private Integer id;

    @ApiModelProperty(value = "设备状态")
    private Integer status;

    @ApiModelProperty(value = "设备备注")
    private String remark;

    @ApiModelProperty(value = "设备创建时间")
    @TableField("create_time")
    private String createTime;

    @ApiModelProperty(value = "设备更新时间")
    @TableField("update_time")
    private String updateTime;

    @ApiModelProperty(value = "设备含有最大充电宝数")
    private String count;

    @ApiModelProperty(value = "店铺ID")
    @TableField("shop_id")
    private Integer shopId;

    @ApiModelProperty(value = "设备图片")
    @TableField("picture")
    private String picture;

    @ApiModelProperty(value = "设备名称")
    private String name;

    @ApiModelProperty(value = "商铺对象")
    @TableField(exist = false)
    private FroShop froShop;

    @ApiModelProperty(value = "充电宝对象")
    @TableField(exist = false)
    private List<FroBattery> children;

}
