package com.battery.eason.pojo;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author HuangSiYuan
 * @since 2021-08-16
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("fro_order")
@ApiModel(value="FroOrder对象", description="")
public class FroOrder implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "唯一标识ID")
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "订单名称")
    private String name;

    @ApiModelProperty(value = "订单金额")
    private String money;

    @ApiModelProperty(value = "订单生成时间")
    @TableField("create_time")
    private String createTime;

    @ApiModelProperty(value = "订单结束时间")
    @TableField("end_time")
    private String endTime;

    @ApiModelProperty(value = "关联用户ID")
    @TableField("user_id")
    private Integer userId;

    @ApiModelProperty(value = "充电宝ID")
    @TableField("battery_id")
    private Integer batteryId;

    @ApiModelProperty(value = "充电宝备注")
    private String remark;

    @ApiModelProperty(value = "订单编号")
    @TableField("order_id")
    private String orderId;

    @ApiModelProperty(value = "订单状态")
    @TableField("status")
    private Integer status;

    @ApiModelProperty(value = "订单绑定用户")
    @TableField(exist = false)
    private FroUser froUser;


}
