package com.battery.eason.pojo;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author HuangSiYuan
 * @since 2021-08-16
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("fro_question")
@ApiModel(value="FroQuestion对象", description="")
public class FroQuestion implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "标识ID")
    private Integer id;

    @ApiModelProperty(value = "问题")
    private String question;

    @ApiModelProperty(value = "问题答案")
    private String answer;

    @ApiModelProperty(value = "类型")
    private String type;

    @ApiModelProperty(value = "父类型")
    @TableField("father_type")
    private String fatherType;

    @ApiModelProperty(value = "图片路径")
    private String picture;


}
