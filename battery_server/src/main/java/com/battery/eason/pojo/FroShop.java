package com.battery.eason.pojo;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author HuangSiYuan
 * @since 2021-08-16
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("fro_shop")
@ApiModel(value="FroShop对象", description="")
public class FroShop implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "唯一标识ID")
    private Integer id;

    @ApiModelProperty(value = "店铺名称")
    private String name;

    @ApiModelProperty(value = "店铺创建时间")
    @TableField("create_time")
    private String createTime;

    @ApiModelProperty(value = "店铺更新时间")
    @TableField("update_time")
    private String updateTime;

    @ApiModelProperty(value = "店铺地址")
    private String address;

    @ApiModelProperty(value = "经度")
    private String longitude;

    @ApiModelProperty(value = "纬度")
    private String latitude;

    @ApiModelProperty(value = "店铺备注")
    private String remark;

    @ApiModelProperty(value = "店铺图片")
    private String picture;

    @ApiModelProperty(value = "店铺状态")
    private Integer status;




}
