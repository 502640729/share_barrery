package com.battery.eason.pojo;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author HuangSiYuan
 * @since 2021-08-16
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("sys_basic")
@ApiModel(value="SysBasic对象", description="")
public class SysBasic implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "标识ID")
    private Integer id;

    @ApiModelProperty(value = "公司名称")
    private String title;

    @ApiModelProperty(value = "公司描述")
    private String description;

    @ApiModelProperty(value = "联系人")
    private String phone;

    @ApiModelProperty(value = "邮箱")
    private String email;

    @ApiModelProperty(value = "地址")
    private String address;

    @ApiModelProperty(value = "图片地址")
    private String logo;

    @ApiModelProperty(value = "项目名称")
    @TableField("web_name")
    private String webName;


}
