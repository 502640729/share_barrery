package com.battery.eason.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 后台用户登录实体类
 *
 * @author HuangSiYuan
 * @since 2021-08-16
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class SysLoginParams {

    private String uuid;

    private String username;

    private String password;

    private String captcha;

    private String phone;

}
