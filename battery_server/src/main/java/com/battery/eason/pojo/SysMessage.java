package com.battery.eason.pojo;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author HuangSiYuan
 * @since 2021-08-16
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("sys_message")
@ApiModel(value="SysMessage对象", description="")
public class SysMessage implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "系统消息唯一标识")
    private Integer id;

    @ApiModelProperty(value = "系统消息")
    private String message;

    @ApiModelProperty(value = "关联管理员")
    @TableField("user_id")
    private Integer userId;

    @ApiModelProperty(value = "消息备注信息")
    private String remark;

    @ApiModelProperty(value = "消息创建时间")
    @TableField("create_time")
    private String createTime;

    @ApiModelProperty(value = "消息更新时间")
    @TableField("update_time")
    private String updateTime;

    @ApiModelProperty(value = "消息标题")
    private String title;

    @ApiModelProperty(value = "消息状态")
    private Integer status;

    @ApiModelProperty(value = "消息分类ID")
    @TableField("tag_id")
    private Integer tagId;

    @ApiModelProperty(value = "关联管理员")
    @TableField(exist = false)
    private SysUser sysUser;

    @ApiModelProperty(value = "标签分类")
    @TableField(exist = false)
    private SysTag sysTag;


}
