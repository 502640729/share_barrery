package com.battery.eason.pojo;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author HuangSiYuan
 * @since 2021-08-16
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("sys_read")
@ApiModel(value="SysRead对象", description="")
public class SysRead implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "用户读取消息表唯一标识")
    private Integer id;

    @ApiModelProperty(value = "已读消息用户")
    @TableField("user_id")
    private Integer userId;

    @ApiModelProperty(value = "消息ID")
    @TableField("message_id")
    private Integer messageId;


}
