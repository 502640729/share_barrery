package com.battery.eason.service;

import com.battery.eason.pojo.RespBean;
import org.springframework.web.multipart.MultipartFile;

/**
 * 基本service层
 *
 * @author HuangSiYuan
 * @since 2021-08-21
 */
public interface BasicService {
    RespBean uploadFile(MultipartFile file);
}
