package com.battery.eason.service;

import com.battery.eason.pojo.FroBattery;
import com.baomidou.mybatisplus.extension.service.IService;
import com.battery.eason.pojo.RespPageBean;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author HuangSiYuan
 * @since 2021-08-16
 */
public interface IFroBatteryService extends IService<FroBattery> {

    RespPageBean pageList(Integer currentPage, Integer pageSize, Integer status, String equipmentName);

}
