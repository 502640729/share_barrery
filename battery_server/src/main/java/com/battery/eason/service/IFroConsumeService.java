package com.battery.eason.service;

import com.battery.eason.pojo.FroConsume;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author HuangSiYuan
 * @since 2021-08-16
 */
public interface IFroConsumeService extends IService<FroConsume> {

}
