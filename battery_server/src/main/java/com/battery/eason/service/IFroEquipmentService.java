package com.battery.eason.service;

import com.battery.eason.pojo.FroEquipment;
import com.baomidou.mybatisplus.extension.service.IService;
import com.battery.eason.pojo.RespPageBean;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author HuangSiYuan
 * @since 2021-08-16
 */
public interface IFroEquipmentService extends IService<FroEquipment> {

    RespPageBean pageList(Integer currentPage, Integer pageSize, String name, Integer status);

    List<FroEquipment> getBatteryByShopId(Integer shopId);
}
