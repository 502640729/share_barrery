package com.battery.eason.service;

import com.battery.eason.pojo.FroOrder;
import com.baomidou.mybatisplus.extension.service.IService;
import com.battery.eason.pojo.RespPageBean;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author HuangSiYuan
 * @since 2021-08-16
 */
public interface IFroOrderService extends IService<FroOrder> {

    RespPageBean listPage(Integer currentPage, Integer pageSize, String username, String name);

    List<FroOrder> getListByUserId(Integer userId);
}
