package com.battery.eason.service;

import com.battery.eason.pojo.FroShop;
import com.baomidou.mybatisplus.extension.service.IService;
import com.battery.eason.pojo.RespPageBean;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author HuangSiYuan
 * @since 2021-08-16
 */
public interface IFroShopService extends IService<FroShop> {

    RespPageBean listPage(Integer currentPage, Integer pageSize, Integer status, String name, String address);

}
