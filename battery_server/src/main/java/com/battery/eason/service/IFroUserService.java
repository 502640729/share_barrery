package com.battery.eason.service;

import com.battery.eason.pojo.FroUser;
import com.baomidou.mybatisplus.extension.service.IService;
import com.battery.eason.pojo.RespBean;
import com.battery.eason.pojo.RespPageBean;
import com.battery.eason.pojo.SysLoginParams;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author HuangSiYuan
 * @since 2021-08-16
 */
public interface IFroUserService extends IService<FroUser> {

    RespPageBean pageList(Integer currentPage, Integer pageSize, String name, String phone, Integer status);

    RespBean login(SysLoginParams loginParams, HttpServletRequest request);

    RespBean uploadAvatar(MultipartFile file, Integer id);
}
