package com.battery.eason.service;

import com.battery.eason.pojo.RespPageBean;
import com.battery.eason.pojo.SysMessage;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author HuangSiYuan
 * @since 2021-08-16
 */
public interface ISysMessageService extends IService<SysMessage> {

    RespPageBean listPage(Integer currentPage, Integer pageSize, String title, String username);

}
