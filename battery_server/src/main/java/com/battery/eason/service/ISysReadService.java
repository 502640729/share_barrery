package com.battery.eason.service;

import com.battery.eason.pojo.SysRead;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author HuangSiYuan
 * @since 2021-08-16
 */
public interface ISysReadService extends IService<SysRead> {

}
