package com.battery.eason.service;

import com.battery.eason.pojo.RespBean;
import com.battery.eason.pojo.RespPageBean;
import com.battery.eason.pojo.SysLoginParams;
import com.battery.eason.pojo.SysUser;
import com.baomidou.mybatisplus.extension.service.IService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author HuangSiYuan
 * @since 2021-08-16
 */
public interface ISysUserService extends IService<SysUser> {

    SysUser getSysUserByUsername(String username);

    RespBean login(SysLoginParams loginParams, HttpServletRequest request);

    RespPageBean pageList(Integer currentPage, Integer pageSize, String username, String phone, Integer status);
}
