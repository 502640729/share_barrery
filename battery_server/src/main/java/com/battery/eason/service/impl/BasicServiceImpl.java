package com.battery.eason.service.impl;

import com.battery.eason.pojo.FileInfo;
import com.battery.eason.pojo.RespBean;
import com.battery.eason.service.BasicService;
import com.battery.eason.utils.DateTimeUtil;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.util.Locale;
import java.util.Random;
import java.util.UUID;

/**
 * 基本Service层
 *
 * @author HuangSiYuan
 * @since 2021-08-21
 */
@Service
public class BasicServiceImpl implements BasicService {

    @Value("${upload.baseUrl}")
    private String baseUrl;

    @Override
    public RespBean uploadFile(MultipartFile imgFile) {
        String uuid = UUID.randomUUID().toString().toUpperCase(Locale.ROOT).replaceAll("-","");
        String realFileName = uuid + "." + imgFile.getOriginalFilename().split("\\.")[1];
        try {

//            File file = new File( "/data/share_battery/upload" + File.separator+realFileName);
//            File file = new File( "S:\\imgFile" + File.separator+realFileName);
            File file = new File( baseUrl + File.separator+realFileName);
            if (!file.getParentFile().exists()) {
                file.getParentFile().mkdirs();
            }
            imgFile.transferTo(file);
            FileInfo fileInfo = new FileInfo();
            fileInfo.setFileName(realFileName);
            fileInfo.setPath(file.getPath());
            fileInfo.setCreateTime(DateTimeUtil.getSysTime());
            return RespBean.success("文件上传成功!",fileInfo);
        } catch (Exception e) {
            e.printStackTrace();
            return RespBean.error("文件上传失败!");
        }
    }

}
