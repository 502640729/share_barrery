package com.battery.eason.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.battery.eason.pojo.FroBattery;
import com.battery.eason.mapper.FroBatteryMapper;
import com.battery.eason.pojo.RespPageBean;
import com.battery.eason.service.IFroBatteryService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author HuangSiYuan
 * @since 2021-08-16
 */
@Service
public class FroBatteryServiceImpl extends ServiceImpl<FroBatteryMapper, FroBattery> implements IFroBatteryService {

    @Autowired
    private FroBatteryMapper froBatteryMapper;

    @Override
    public RespPageBean pageList(Integer currentPage, Integer pageSize, Integer status,String equipmentName) {
        RespPageBean respPageBean = new RespPageBean();
        Page<FroBattery> page = new Page<>(currentPage,pageSize);
        IPage<FroBattery> iPage = froBatteryMapper.getBatteryByPage(page,status,equipmentName);
        respPageBean.setData(iPage.getRecords());
        respPageBean.setTotal(iPage.getTotal());
        return respPageBean;
    }
}
