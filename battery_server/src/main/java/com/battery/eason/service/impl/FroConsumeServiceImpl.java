package com.battery.eason.service.impl;

import com.battery.eason.pojo.FroConsume;
import com.battery.eason.mapper.FroConsumeMapper;
import com.battery.eason.service.IFroConsumeService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author HuangSiYuan
 * @since 2021-08-16
 */
@Service
public class FroConsumeServiceImpl extends ServiceImpl<FroConsumeMapper, FroConsume> implements IFroConsumeService {

}
