package com.battery.eason.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.battery.eason.pojo.FroEquipment;
import com.battery.eason.mapper.FroEquipmentMapper;
import com.battery.eason.pojo.RespPageBean;
import com.battery.eason.service.IFroEquipmentService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author HuangSiYuan
 * @since 2021-08-16
 */
@Service
public class FroEquipmentServiceImpl extends ServiceImpl<FroEquipmentMapper, FroEquipment> implements IFroEquipmentService {

    @Autowired
    private FroEquipmentMapper froEquipmentMapper;

    @Override
    public RespPageBean pageList(Integer currentPage, Integer pageSize, String name, Integer status) {
        RespPageBean respPageBean = new RespPageBean();
        Page<FroEquipment> equipmentPage = new Page<>(currentPage,pageSize);
        IPage<FroEquipment> froEquipmentIPage = froEquipmentMapper.getEquipmentByPage(equipmentPage,name,status);
        respPageBean.setTotal(froEquipmentIPage.getTotal());
        respPageBean.setData(froEquipmentIPage.getRecords());
        return respPageBean;
    }

    @Override
    public List<FroEquipment> getBatteryByShopId(Integer shopId) {
        return froEquipmentMapper.getBattery(shopId);
    }
}
