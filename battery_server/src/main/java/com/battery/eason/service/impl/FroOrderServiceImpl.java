package com.battery.eason.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.battery.eason.pojo.FroOrder;
import com.battery.eason.mapper.FroOrderMapper;
import com.battery.eason.pojo.RespPageBean;
import com.battery.eason.service.IFroOrderService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author HuangSiYuan
 * @since 2021-08-16
 */
@Service
public class FroOrderServiceImpl extends ServiceImpl<FroOrderMapper, FroOrder> implements IFroOrderService {

    @Autowired
    private FroOrderMapper froOrderMapper;

    @Override
    public RespPageBean listPage(Integer currentPage, Integer pageSize, String username, String name) {
        RespPageBean respPageBean = new RespPageBean();
        Page<FroOrder> page = new Page<>(currentPage,pageSize);
        IPage<FroOrder> iPage = froOrderMapper.listOrderPage(page,username,name);
        respPageBean.setData(iPage.getRecords());
        respPageBean.setTotal(iPage.getTotal());
        return respPageBean;
    }

    @Override
    public List<FroOrder> getListByUserId(Integer userId) {
        return froOrderMapper.getListByUserId(userId);
    }
}
