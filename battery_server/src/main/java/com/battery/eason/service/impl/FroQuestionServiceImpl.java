package com.battery.eason.service.impl;

import com.battery.eason.pojo.FroQuestion;
import com.battery.eason.mapper.FroQuestionMapper;
import com.battery.eason.service.IFroQuestionService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author HuangSiYuan
 * @since 2021-08-16
 */
@Service
public class FroQuestionServiceImpl extends ServiceImpl<FroQuestionMapper, FroQuestion> implements IFroQuestionService {

}
