package com.battery.eason.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.battery.eason.pojo.FroShop;
import com.battery.eason.mapper.FroShopMapper;
import com.battery.eason.pojo.RespPageBean;
import com.battery.eason.service.IFroShopService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.util.pattern.PathPattern;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author HuangSiYuan
 * @since 2021-08-16
 */
@Service
public class FroShopServiceImpl extends ServiceImpl<FroShopMapper, FroShop> implements IFroShopService {

    @Autowired
    private FroShopMapper froShopMapper;

    @Override
    public RespPageBean listPage(Integer currentPage, Integer pageSize, Integer status, String name, String address) {
        RespPageBean respPageBean = new RespPageBean();
        Page<FroShop> page = new Page<>(currentPage,pageSize);
        IPage<FroShop> iPage = froShopMapper.listPage(page,status,name,address);
        respPageBean.setData(iPage.getRecords());
        respPageBean.setTotal(iPage.getTotal());
        return respPageBean;
    }
}
