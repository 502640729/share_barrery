package com.battery.eason.service.impl;

import com.battery.eason.pojo.SysBasic;
import com.battery.eason.mapper.SysBasicMapper;
import com.battery.eason.service.ISysBasicService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author HuangSiYuan
 * @since 2021-08-16
 */
@Service
public class SysBasicServiceImpl extends ServiceImpl<SysBasicMapper, SysBasic> implements ISysBasicService {

}
