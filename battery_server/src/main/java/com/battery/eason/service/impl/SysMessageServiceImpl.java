package com.battery.eason.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.battery.eason.pojo.RespPageBean;
import com.battery.eason.pojo.SysMessage;
import com.battery.eason.mapper.SysMessageMapper;
import com.battery.eason.service.ISysMessageService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author HuangSiYuan
 * @since 2021-08-16
 */
@Service
public class SysMessageServiceImpl extends ServiceImpl<SysMessageMapper, SysMessage> implements ISysMessageService {

    @Autowired
    private SysMessageMapper sysMessageMapper;

    @Override
    public RespPageBean listPage(Integer currentPage, Integer pageSize, String title, String username) {
        RespPageBean respPageBean = new RespPageBean();
        Page<SysMessage> page = new Page<>(currentPage,pageSize);
        IPage<SysMessage> iPage = sysMessageMapper.messageByPage(page,title,username);
        respPageBean.setTotal(iPage.getTotal());
        respPageBean.setData(iPage.getRecords());
        return respPageBean;

    }
}
