package com.battery.eason.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.battery.eason.pojo.RespPageBean;
import com.battery.eason.pojo.SysRole;
import com.battery.eason.mapper.SysRoleMapper;
import com.battery.eason.service.ISysRoleService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.util.pattern.PathPattern;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author HuangSiYuan
 * @since 2021-08-16
 */
@Service
public class SysRoleServiceImpl extends ServiceImpl<SysRoleMapper, SysRole> implements ISysRoleService {

    @Autowired
    private SysRoleMapper sysRoleMapper;

    @Override
    public RespPageBean listPage(Integer currentPage, Integer pageSize, String name) {
        RespPageBean respPageBean = new RespPageBean();
        Page<SysRole> page = new Page<>(currentPage,pageSize);
        IPage<SysRole> IPage = sysRoleMapper.listPage(page,name);
        respPageBean.setTotal(IPage.getTotal());
        respPageBean.setData(IPage.getRecords());
        return respPageBean;
    }
}
