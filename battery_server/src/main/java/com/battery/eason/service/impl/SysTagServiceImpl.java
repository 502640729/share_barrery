package com.battery.eason.service.impl;

import com.battery.eason.pojo.SysTag;
import com.battery.eason.mapper.SysTagMapper;
import com.battery.eason.service.ISysTagService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author HuangSiYuan
 * @since 2021-08-16
 */
@Service
public class SysTagServiceImpl extends ServiceImpl<SysTagMapper, SysTag> implements ISysTagService {

}
