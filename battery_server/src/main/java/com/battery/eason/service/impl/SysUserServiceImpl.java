package com.battery.eason.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.battery.eason.config.JwtTokenUtil;
import com.battery.eason.pojo.RespBean;
import com.battery.eason.pojo.RespPageBean;
import com.battery.eason.pojo.SysLoginParams;
import com.battery.eason.pojo.SysUser;
import com.battery.eason.mapper.SysUserMapper;
import com.battery.eason.service.ISysUserService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.battery.eason.utils.MD5Util;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpRequest;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author HuangSiYuan
 * @since 2021-08-16
 */
@Service
public class SysUserServiceImpl extends ServiceImpl<SysUserMapper, SysUser> implements ISysUserService {

    @Autowired
    private SysUserMapper sysUserMapper;

    @Autowired
    private JwtTokenUtil jwtTokenUtil;

    @Override
    public SysUser getSysUserByUsername(String username) {
        /*第一个参数为用户名,第二个是为了保证用户不被锁定*/
        return sysUserMapper.selectOne(new QueryWrapper<SysUser>().eq("username",username).eq("status",true));
    }

    @Override
    public RespBean login(SysLoginParams loginParams, HttpServletRequest request) {
        System.out.println(loginParams.toString());
        System.out.println(request.getServletContext().getAttribute(loginParams.getUuid()));
        //校验验证码
        if (!request.getServletContext().getAttribute(loginParams.getUuid()).equals(loginParams.getCaptcha())) {
            return RespBean.error("验证码不正确!");
        }
        //校验密码
        SysUser sysUser = sysUserMapper.selectOne(new QueryWrapper<SysUser>().eq("username",loginParams.getUsername()));
        if (!sysUser.getPassword().equals(MD5Util.md5(loginParams.getPassword()))) {
            return RespBean.error("用户名或密码不正确!");
        }
        //校验账户是否被禁用
        if (sysUser.getStatus() == 0) {
            return RespBean.error("该账户被禁用!");
        }
        //设置token返回前端
        String token = jwtTokenUtil.generateToken(sysUser);
        Map<String,Object> map = new HashMap<>();
        map.put("token", token);
        //用户登录成功将用户信息存储到session里面
        request.getServletContext().setAttribute(loginParams.getUsername(),sysUser);
        request.getServletContext().removeAttribute(loginParams.getUuid());
        return RespBean.success("用户登录成功!",map);
    }

    @Override
    public RespPageBean pageList(Integer currentPage, Integer pageSize, String username, String phone, Integer status) {
        RespPageBean respPageBean = new RespPageBean();
        Page<SysUser> userPage = new Page<>(currentPage,pageSize);
        IPage<SysUser> sysUserIPage = sysUserMapper.getSysUserByPage(userPage,username,phone,status);
        respPageBean.setData(sysUserIPage.getRecords());
        respPageBean.setTotal(userPage.getTotal());
        return respPageBean;
    }
}