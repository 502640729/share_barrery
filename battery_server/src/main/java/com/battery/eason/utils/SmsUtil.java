package com.battery.eason.utils;

import net.sf.json.JSONObject;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

public class SmsUtil {
    /**
     * 接口请求地址
     */
    private static final String URL =
      "http://v.juhe.cn/sms/send?mobile=%s&tpl_id=%s&tpl_value=%s&key=%s";
    /**
     * 申请接口的请求key
     * */
    private static final String KEY = "e4b817d943115d81d68362bdc9961a51";
    /**
     * 短信模板ID
     * */
    private static final int TPL_ID = 81762;

    public static void main(String[] args) {
        //用于接收短信的手机号码，你需要修改此处
        String mobile = "19851622703";
        //短信模板中的您自定义的变量
        String variable = "#code#="+randomCode();
        String code = randomCode();
        System.out.println(sendMsg(mobile,code));

    }

    public static String randomCode(){
        String code="";
        for(int i=0;i<4;i++){
            Random random=new Random();
            code+=random.nextInt(10);
        }
        return code;
    }


    public static Map<String,Object> sendMsg(String mobile,String code){
        //发送http请求的url
        String url = null;
        try {
            url = String.format(URL, mobile, TPL_ID, URLEncoder.encode("#code#="+code, "utf-8"), KEY);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        String response = doGet(url);
        JSONObject jsonObject = JSONObject.fromObject(response);
        Map<String,Object> map=new HashMap<>();
        /*error_code为0表示发送成功，其他都为失败*/
        map.put("error_code",jsonObject.getInt("error_code"));
        /*短信发送成功或发送失败原因*/
        map.put("reason",jsonObject.getString("reason"));
        /*封装发送信息信息*/
        Map<String,Object> result=new HashMap<>();
        /*只有发送成功时才有发送返回值*/
        if(jsonObject.getInt("error_code")==0){
            JSONObject resultJson=jsonObject.getJSONObject("result");
            /*发送成功数量*/
            result.put("count",resultJson.getInt("count"));
            /*扣除数量*/
            result.put("fee",resultJson.getInt("fee"));
            /*短信ID*/
            result.put("sid",resultJson.getString("sid"));
        }
        map.put("result",result);
        return map;
    }


    /**
     * 打印请求结果
     *
     * @param mobile   手机号
     * @param variable 模板变量
     */
    private static void print(String mobile, String variable) {
        //发送http请求的url
        String url = null;
        try {
            url = String.format(URL, mobile, TPL_ID, URLEncoder.encode(variable, "utf-8"), KEY);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        String response = doGet(url);
        System.out.println(response);
        try {
            JSONObject jsonObject = JSONObject.fromObject(response);
            int error_code = jsonObject.getInt("error_code");
            if (error_code == 0) {
                System.out.println("调用接口成功");
                JSONObject result = jsonObject.getJSONObject("result");
                String sid = result.getString("sid");
                int fee = result.getInt("fee");
                System.out.println("本次发送的唯一标示：" + sid);
                System.out.println("本次发送消耗的次数：" + fee);
            }else{
                System.out.println("调用接口失败："+ jsonObject.getString("reason"));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * get方式的http请求
     *
     * @param httpUrl 请求地址
     * @return 返回结果
     */
    private static String doGet(String httpUrl) {
        HttpURLConnection connection = null;
        InputStream inputStream = null;
        BufferedReader bufferedReader = null;
        // 返回结果字符串
        String result = null;
        try {
            // 创建远程url连接对象
            URL url = new URL(httpUrl);
            // 通过远程url连接对象打开一个连接，强转成httpURLConnection类
            connection = (HttpURLConnection) url.openConnection();
            // 设置连接方式：get
            connection.setRequestMethod("GET");
            // 设置连接主机服务器的超时时间：15000毫秒
            connection.setConnectTimeout(15000);
            // 设置读取远程返回的数据时间：60000毫秒
            connection.setReadTimeout(60000);
            // 发送请求
            connection.connect();
            // 通过connection连接，获取输入流
            if (connection.getResponseCode() == 200) {
                inputStream = connection.getInputStream();
                // 封装输入流，并指定字符集
                bufferedReader = new BufferedReader(new InputStreamReader(inputStream, "UTF-8"));
                // 存放数据
                StringBuilder sbf = new StringBuilder();
                String temp;
                while ((temp = bufferedReader.readLine()) != null) {
                    sbf.append(temp);
                    sbf.append(System.getProperty("line.separator"));
                }
                result = sbf.toString();
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            // 关闭资源
            if (null != bufferedReader) {
                try {
                    bufferedReader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (null != inputStream) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (connection != null) {
                connection.disconnect();// 关闭远程连接
            }
        }
        return result;
    }


    /**
     * post方式的http请求
     *
     * @param httpUrl 请求地址
     * @param param   请求参数
     * @return 返回结果
     */
    private static String doPost(String httpUrl, String param) {
        HttpURLConnection connection = null;
        InputStream inputStream = null;
        OutputStream outputStream = null;
        BufferedReader bufferedReader = null;
        String result = null;
        try {
            URL url = new URL(httpUrl);
            // 通过远程url连接对象打开连接
            connection = (HttpURLConnection) url.openConnection();
            // 设置连接请求方式
            connection.setRequestMethod("POST");
            // 设置连接主机服务器超时时间：15000毫秒
            connection.setConnectTimeout(15000);
            // 设置读取主机服务器返回数据超时时间：60000毫秒
            connection.setReadTimeout(60000);
            // 默认值为：false，当向远程服务器传送数据/写数据时，需要设置为true
            connection.setDoOutput(true);
            // 设置传入参数的格式:请求参数应该是 name1=value1&name2=value2 的形式。
            connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            // 通过连接对象获取一个输出流
            outputStream = connection.getOutputStream();
            // 通过输出流对象将参数写出去/传输出去,它是通过字节数组写出的
            outputStream.write(param.getBytes());
            // 通过连接对象获取一个输入流，向远程读取
            if (connection.getResponseCode() == 200) {
                inputStream = connection.getInputStream();
                // 对输入流对象进行包装:charset根据工作项目组的要求来设置
                bufferedReader = new BufferedReader(new InputStreamReader(inputStream, "UTF-8"));
                StringBuilder sbf = new StringBuilder();
                String temp;
                // 循环遍历一行一行读取数据
                while ((temp = bufferedReader.readLine()) != null) {
                    sbf.append(temp);
                    sbf.append(System.getProperty("line.separator"));
                }
                result = sbf.toString();
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            // 关闭资源
            if (null != bufferedReader) {
                try {
                    bufferedReader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (null != outputStream) {
                try {
                    outputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (null != inputStream) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (connection != null) {
                connection.disconnect();
            }
        }
        return result;
    }
}
